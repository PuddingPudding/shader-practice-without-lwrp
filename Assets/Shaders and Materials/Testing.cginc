#ifndef TESTING_CGINC //若還未定義，那就進行定義動作
//(之後如果有其他類別也做了相同的定義，則你在引入的時候他就只會去引入第一個cginc檔)
#define TESTING_CGINC

float4 GetTestValue()
{
	return float4(1, 0.5, 0.5, 1);
}

#endif