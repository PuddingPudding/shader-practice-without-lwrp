﻿Shader "Unlit/Click Ripple Shader"
{
	/*
	水波紋Shader，參考自:https://www.itread01.com/content/1545840210.html
	*/
	Properties
	{
		[PerRendererData]_MainTex("Sprite Texture", 2D) = "white" {}//令Texture輸入不用在Material中處理
		[HideInInspector]_StartTime("Start Time",float) = 0  //起始時間，用普通程式來控制
		_AnimationTime("AnimationTime",range(0.1,10.0)) = 1.5 //動畫時間
		_Width("Width",range(0.1,3.0)) = 0.3   //內圓和外圓形成環的寬度
		_StartWidth("Start Width",range(0,1.0)) = 0.3  //內圓的預設直徑，注意這裡是按照uv尺寸走的，最大也只到1
		_StartPosX("Start Pos X" , range(0,1.0)) = 0.5
		_StartPosY("Start Pos Y" , range(0,1.0)) = 0.5
		[Toggle]_isAlpha("isAlpha",float) = 1
		[Toggle]_isColorShift("isColorShift",float) = 1
		[MaterialToggle]PixelSnap("Pixe Snap",float) = 1
	}
		SubShader
		{
			Tags{"Queue" = "Transparent" "RenderType" = "Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma target 3.0 //調整Shader的編譯等集，參考:https://docs.unity3d.com/Manual/SL-ShaderCompileTargets.html
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float2 texcoord:TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				//sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D _MainTex;
				float _StartTime;
				float _AnimationTime;
				float _StartWidth;
				float _StartPosX;
				float _StartPosY;
				float _Width;
				float _isAlpha;
				float _isColorShift;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					//o.vertex = UnityObjectToClipPos(v.vertex + float4(0.5, 0 , 0.5, 0)); //依照原本物體外觀往右下移動
					//o.vertex = UnityObjectToClipPos(v.vertex) + float4(0.2, -0.1, 0, 0); //視覺上往右上移動(顯示時的Y軸由上往下為增加)
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					o.texcoord = v.uv;
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				//我看不太懂這個顏色轉換在幹啥，shift的xyz值目前側起來
				//x似乎代表色盤上(HSV格式)轉幾度(180會轉到正對面，對比度最高的顏色)
				//y好像是飽和度
				//z則是亮度，調到0的時候會整個全黑
				fixed3 shift_col(fixed3 RGB, half3 shift)
				{
					fixed3 RESULT = fixed3(RGB);
					float VSU = shift.z*shift.y*cos(shift.x*3.14159265 / 180); //*圓周率/180，相當於角度轉換成弧度
					float VSW = shift.z*shift.y*sin(shift.x*3.14159265 / 180);

					RESULT.x = (.299*shift.z + .701*VSU + .168*VSW)*RGB.x
						+ (.587*shift.z - .587*VSU + .330*VSW)*RGB.y
						+ (.114*shift.z - .114*VSU - .497*VSW)*RGB.z;

					RESULT.y = (.299*shift.z - .299*VSU - .328*VSW)*RGB.x
						+ (.587*shift.z + .413*VSU + .035*VSW)*RGB.y
						+ (.114*shift.z - .114*VSU + .292*VSW)*RGB.z;

					RESULT.z = (.299*shift.z - .3*VSU + 1.25*VSW)*RGB.x
						+ (.587*shift.z - .588*VSU - 1.05*VSW)*RGB.y
						+ (.114*shift.z + .886*VSU - .203*VSW)*RGB.z;

					return RESULT;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 color = tex2D(_MainTex,i.uv);
				//float2 pos = (i.texcoord - float2(0.5,0.5)) * 2; //計算畫素點到中心的距離，乘以2就當做圓的直徑吧。
				//網路上的教學是直接用TexCoord，這邊想說改用uv看看，因為感覺上兩者一樣 (實驗結果相同)
				float2 pos = (i.uv - float2(_StartPosX , _StartPosY)) /** 2*/; //先試著拿掉*2，用半徑來算
				//(該像素點的uv位置-圓心)*2 ->得出圓心往該像素點的向量

				//大於最大寬度以及小於0都去掉這部分畫素
				float dis = (_Time.y - _StartTime) / _AnimationTime + _StartWidth - length(pos);
				//計算過了多久/一次演示的時間 + 起始寬度 - 位置距離圓心點的長度(2倍)
				//這邊主要算的是你這個點到波紋鋒頭的距離

				//return float4 ( ((sin(_Time.y*5)+1)/2 ).xxx, 1);
				//return float4(( ( (_SinTime.w + 1) / 2) * 5).xxx, 1);
				if (dis<0 || dis>_Width)
				{
				  return fixed4(0,0,0,0);
				}

				//如果開啟了透明度漸變就讓透明度進行插值
				float alpha = 1;
				if (_isAlpha == 1)
				{
					alpha = clamp((_Width - dis) * 3,0.1,1.5); //clamp會回傳界在後面兩個參數中的數字
					//clamp(x,a,b)，x在a~b之間就回傳x，<a回傳a，>b回傳b

				}

				fixed3 shiftColor = color;
				if (_isColorShift == 1)
				{
					//half3 shift = half3(_Time.w * 10,1,1); //隨時間轉換色相
					float targetSinTime = sin(_Time.y * 5) * 0.5 + 0.5;
					float3 shift = float3(0, targetSinTime , 1); //隨時間去調整顏色飽和度
					//float3 shift = float3(0, 1, targetSinTime); //隨時間去調整明度
					shiftColor = shift_col(color,shift);
				}

				return fixed4(shiftColor,color.a*alpha);
			}
			ENDCG
		}

		}
}
