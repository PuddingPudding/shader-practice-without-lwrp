﻿Shader "Unlit/Dissolve Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture" , 2D) = "white"{}
		[Header(Style)]
		[Enum(EDissipateStyle)]  _CustomStyle("Custom Style", Float) = 0
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("CullMode", float) = 0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		//Toggle標籤會讓該屬性在Insepector當中變得像布林值一樣可以勾選，勾起來代表1，空著則代表0
		//在程式碼裡頭可以用屬性_ON來判定他是否有勾選(以這邊為例的話就是PixelSnap_ON)
		_EdgeColour1("Edge colour 1", Color) = (1.0, 1.0, 1.0, 1.0)
		_EdgeColour2("Edge colour 2", Color) = (1.0, 1.0, 1.0, 1.0)
		_Level("Dissolution level", Range(0.0, 1.0)) = 0.1
		_Edges("Edge width", Range(0.0, 1.0)) = 0.1
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType" = "Transparent"  }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			//常見的混和(Blend)模式，由現在(此頂點的顏色*Alpha值) + (後面其他點顏色*1-Alpha值)算出
			Cull [_CullMode] //三種Cull(Back/Front/Off)分別代表剃除Back(物體背面不渲染，從我們看過去的角度為準，平常是這個模式……吧)
			//Front則是正面不渲染，讓你看到背面，Off就是都渲染

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
			sampler2D _NoiseTex;
            float4 _MainTex_ST;
			float _Level;
			float _Edges;
			int _CustomStyle;
			float4 _EdgeColour1;
			float4 _EdgeColour2;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				float dissolveValue = tex2D(_NoiseTex, i.uv).r;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
				
				col.a *= step(_Level+0.01, dissolveValue);
				//如果消散風格調成Dissolve的話就會另外再考慮邊界的特效

				if (dissolveValue < col.a && dissolveValue < _Level + _Edges && _CustomStyle == 1)
				{
					col = lerp(_EdgeColour1, _EdgeColour2 , (dissolveValue - _Level) / _Edges);
				}

                return col;
            }
            ENDCG
        }
    }
}
