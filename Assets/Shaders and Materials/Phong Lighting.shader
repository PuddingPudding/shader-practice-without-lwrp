﻿Shader "Unlit/Phong Lighting"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_AmbientClr("Ambient Color" , Color) = (1,1,1,1) //宣告有個環境光色彩，預設顏色為純白(rgba->1,1,1,1)
		_AmbientStrength("Ambient Strength" , float) = 0.1 //宣告環境光強度，預設為0.1
		_Gloss("Gloss" , float) = 1 //讓外界可以調整光澤度
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

            struct appdata//傳給頂點Shader的資料
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal:NORMAL; //宣告說要跟物體取得法向量
            };

            struct v2f//頂點Shader再傳給平面Shader的資料
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 normal:NORMAL; //宣告說將法向量存起來
				float3 worldPos:WORLD_POSITION; //我第一次看到的時候是寫TEXCOORD2
				//但我後來發現v2f這邊變數冒號後面的部分其實可以隨便打
            };
            sampler2D _MainTex;
            float4 _MainTex_ST;

			//在Shader的Pass當中再次宣告要取用的變數
			//特別注意Pass裡頭的顏色型態不能直接打"Color"，而是"float4"，代表四個浮點數構成的一組資料
			float4 _AmbientClr; //你也可以用之前看到的fixed4，占用的記憶體較少(不過精度也較低)
			float _AmbientStrength;

			float _Gloss;

            v2f vert (appdata v) //頂點Shader
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex );
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.normal = v.normal;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex); //將頂點資料轉換成世界座標
                return o;
            }

            fixed4 frag (v2f i) : SV_Target //預設的Fragment Shader
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv); //之前看到的基本顏色(依照UV座標從貼圖上取樣)
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				float4 ambient = _AmbientClr * _AmbientStrength; //計算環境光的色彩

				float3 lightSrc = _WorldSpaceLightPos0.xyz; //取得平行光源方向
				//特別注意，這邊LightPos0並不是給你位置，而是物體看相光源的方向
				float3 lightSrcClr = _LightColor0.rgb; //取得平行光源的顏色
				//特別注意，_LightColor0需要你引入"UnityLightingCommon.cginc"函式庫才能用
				float3 normalVector = i.normal;
				normalVector = normalize(normalVector);
				float diffuseLightStrength = dot(lightSrc, normalVector); //計算目前頂點的法向量與光照向量的重疊度
				diffuseLightStrength = max(0, diffuseLightStrength); //設定漫射光強度最低至少為0
				float4 diffuse = float4(lightSrcClr * diffuseLightStrength, 1); //計算漫射光色彩
				
				float3 camPos = _WorldSpaceCameraPos; //取得Camera位置
				float3 viewDir = normalize(camPos - i.worldPos); //求出物體看向Camera的單位向量
				float3 reflectDir = reflect(-lightSrc, normalVector); //取得反射向量 (帶入"看向物體的角度"和"表面法向量")
				float specularStrength = dot(reflectDir, viewDir); //反射向量與視線的重疊度，用來計算光反射過去的強度
				specularStrength = max(0, specularStrength); //跟先前Diffuse一樣，限制強度最小只到0
				specularStrength = pow(specularStrength, _Gloss); //額外加入光澤度
				float4 specular = float4( lightSrcClr * specularStrength , 1);
				
				//col *= (ambient + diffuse + specular);

				col *= (ambient + diffuse);
				col += specular; //將高光直接附加在先前光照混和後的顏色上，這麼一來高光會更顯眼

                return col;
            }
            ENDCG
        }
    }
}

/*return float4(camPos, 1);*/
//
//specularStrength *= 0.5;

//原定的最終計算


				//col += ambient + diffuse + specular;

//測試
				/*return float4(specularStrength.xxx, 1);*/
				//return float4(i.vertex);
				//return float4(i.worldPos, 1);