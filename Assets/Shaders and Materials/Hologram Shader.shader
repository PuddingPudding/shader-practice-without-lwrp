﻿Shader "Unlit/Hologram Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_HoloClr("Hologram Color", Color) = (0,0,1,1)
		_FlowSegment("Flow Segment" , range(1 , 500)) = 20 //切成幾塊
		_FlowSpeed("Flow Speed" , range(0.0 , 200.0)) = 2
		_PrimaryAlpha("Primary Alpha" , range(0.0, 1.0) ) = 1
		_SecondaryAlpha("Secondary Alpha" , range(0.0,1.0) ) = 0.3
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("CullMode", float) = 0
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			//常見的混和(Blend)模式，由現在(此頂點的顏色*Alpha值) + (後面其他點顏色*1-Alpha值)算出
			Cull[_CullMode]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _HoloClr;
			int _FlowSegment;
			float _FlowSpeed;
			float _PrimaryAlpha;
			float _SecondaryAlpha;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				float4 finalClr = col * _HoloClr;
				
				float nowLV = floor( (i.uv.y + _Time.y *_FlowSpeed) * _FlowSegment);//現在渲染的點在第幾層
				finalClr.a = col.a * lerp(_PrimaryAlpha, _SecondaryAlpha, nowLV % 2);
                return finalClr;
            }
            ENDCG
        }
    }
}
