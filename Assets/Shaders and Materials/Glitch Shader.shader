﻿Shader "Unlit/Glitch Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_GlitchSegment("Glitch Segment" , range(0 , 500)) = 50
		_GlitchIntensity("Glitch Intensity" , range(0.0 , 1.0) ) = 0.1
		_GlitchShiftClamp("Glitch Shift Clamp" , range(-0.5,0.5) ) = 0.005
		_GlitchFrequency("Glitch Frequency" , range(0.0 , 25)) = 10
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("CullMode", float) = 0
    }

    SubShader
    {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			//常見的混和(Blend)模式，由現在(此頂點的顏色*Alpha值) + (後面其他點顏色*1-Alpha值)算出
			Cull[_CullMode]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

			float nrand(float x, float y) //網路上看到的隨機function (好像是因為圖像上希望去弄出有點規律的隨機而這麼做)
			{//帶入固定input就會有固定output，這樣子比較有辦法去控制說同區塊的像素點要做出一樣的效果
				return frac(sin(dot(float2(x, y), float2(12.9898, 78.233))) * 43758.5453);
			}

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _GlitchShiftClamp;
			int _GlitchSegment;
			float _GlitchIntensity;
			float _GlitchFrequency;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				/*需要做到
				把整個物體切成N塊(N為GlitchSegment+1)
				每次更新時隨機挑出幾個作偏移
				*/

				float nowLV = floor(i.uv.y * (_GlitchSegment + 1) );
				float randomNum = nrand(floor(_Time.y * _GlitchFrequency) , nowLV);
				float shiftSwitch = step( (1-_GlitchIntensity)  , randomNum);//這一塊是否要偏移

				//float shiftLV = (i.uv.y + /*_Time.y * _GlitchIntensity*/ nrand(_Time.y , _Time.y) )* _GlitchSegment;
				////shiftLV = nrand(/*_Time.y*/ 1, floor(shiftLV) );
				//shiftLV -= floor(shiftLV);
				////return float4(shiftLV.xxx , 1);
				//int shiftSwitch = step(0.5, shiftLV);
				
				//float ran = math.random.range(0, 2);

				float2 samplePoint = i.uv + float2(_GlitchShiftClamp * shiftSwitch , 0);
				float4 finalClr = tex2D(_MainTex, samplePoint);
				return finalClr;
				/*return float4(nrand(_Time.y , i.uv.y).xxx , 1);
                return col;*/
            }
            ENDCG
        }
    }
}
