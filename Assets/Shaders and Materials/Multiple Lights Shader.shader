﻿// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'

Shader "Unlit/Multiple Lights Shader"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Gloss("Gloss", Range(8.0,256)) = 20
		_DiffuseClr("Diffuse Color", Color) = (1,1,1,1)
		_SpecularClr("Specular Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		//Opaque: 用於大多數著色器（法線著色器、自發光著色器、反射著色器以及地形的著色器）。
		//Transparent : 用於半透明著色器（透明著色器、粒子著色器、字體著色器、地形額外通道的著色器）。
		//TransparentCutout : 蒙皮透明著色器（Transparent Cutout，兩個通道的植被著色器）。
		//Background : Skybox shaders.天空盒著色器。
		//Overlay : GUITexture, Halo, Flare shaders.光暈著色器、閃光著色器
		Tags {"RenderType" = "Opaque" }
		LOD 100

		//Base Pass
		Pass
		{
			//用於向前渲染，該Pass會計算環境光，最重要的平行光，逐頂點/SH光源和LightMaps
			Tags{"LightMode" = "ForwardBase"}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			//multi_compile_fwdbase指令可以保證我們在shader中使用光照衰減等光照變量可以被正確賦值
			//#pragma multi_compile_fwdbase
			#include "unitycg.cginc"
			#include "Lighting.cginc"

			float _Gloss;
			float4 _DiffuseClr;
			float4 _SpecularClr;
			float4 _MainTex_ST;
			sampler2D _MainTex;

			struct VertextInput //原本叫appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
				float2 uv :TEX_UV;
			};

			v2f vert(VertextInput v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;				
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				//獲取世界座標下的法線的單位向量
				fixed3 worldNormal = normalize(i.worldNormal);
				//獲取世界座標下的光照方向的單位向量 (頂點往光源)
				fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
				//獲取世界座標下的視角方向的單位向量 (頂點看向camera的向量)
				fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
				//獲取環境光
				//fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz;
				//漫反射光的計算公式
				fixed3 diffuse = _LightColor0.rgb * _DiffuseClr.rgb * max(0, dot(worldNormal, worldLightDir));
				//環境光的計算公式 (Specular，這邊算法有點看不懂，但是要去求出光打在頂點後的反射向量與視角向量的重疊度)
				//(或著該直接說，去看光打在頂點後反射到你眼裡的程度)
				fixed3 halfDir = normalize(worldLightDir + worldViewDir); 
				//fixed3 halfDir = reflect(-worldViewDir, worldNormal);
				//return float4(halfDir, 1);
				fixed3 specular = _LightColor0.rgb * _SpecularClr.rgb * pow(max(0, dot(worldNormal, halfDir)), _Gloss);

				//平行光的距離永遠爲1,不存在光照衰減
				float atten = 1.0;

				/*return fixed4(ambient + (diffuse + specular) * atten, 1.0);*/
				return col * fixed4( (diffuse + specular) * atten, 1.0); //不太喜歡加環境光的效果所以先拿掉
			}
		ENDCG
	}
	//Addtional Pass
	Pass
	{
			//用於向前渲染，該模式代表除場景中最重要的平行光之外的額外光源的處理，每個光源會調用該pass一次
			Tags{"LightMode" = "ForwardAdd"}

			//混合模式，表示該Pass計算的光照結果可以在幀緩存中與之前的光照結果進行疊加，否則會覆蓋之前的光照結果
			Blend One One

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			//multi_compile_fwdadd指令可以保證我們在shader中使用光照衰減等光照變量可以被正確賦值
			#pragma multi_compile_fwdadd
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			float _Gloss;
			float4 _DiffuseClr;
			float4 _SpecularClr;

			struct VertextInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
			};

			v2f vert(VertextInput v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				
				//獲取世界座標下的法線的單位向量
				fixed3 worldNormal = normalize(i.worldNormal);
				//獲取世界座標下的視角方向的單位向量
				fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

				#ifdef USING_DIRECTIONAL_LIGHT  //平行光下可以直接獲取世界座標下的光照方向
					fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
					//return float4(0, 0, 1, 0);//測試，當你的Pass標籤設定LightMode為ForwardBass時會進到這裡
					//(並且是整個物體被影響)
				#else  //其他光源下_WorldSpaceLightPos0代表光源的世界座標，與頂點的世界座標的向量相減可得到世界座標下的光照方向
					fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos.xyz);
					//取得目前該頂點看往其他光源的單位向量
					//return float4(1, 0, 0, 0); //測試，當你的Pass標籤設定LightMode為ForwardAdd時會進到這裡
					//(並且只有被額外光源照到的地方會影響)
				#endif

					//漫反射光的計算公式 (當今天LightMode是ForwardAdd的時候，_LightColor0會變成去取額外光源)
					fixed3 diffuse = _LightColor0.rgb * _DiffuseClr.rgb * max(0, dot(worldNormal, worldLightDir));

					//環境光的計算公式 (基本上跟上面那邊一樣)
					fixed3 halfDir = normalize(worldLightDir + worldViewDir);
					fixed3 specular = _LightColor0.rgb * _SpecularClr.rgb * pow(max(0, dot(worldNormal, halfDir)), _Gloss);

					#ifdef USING_DIRECTIONAL_LIGHT  //平行光下不存在光照衰減，恆值爲1
						fixed atten = 1.0;
					#else
						#if defined (POINT)    //點光源的光照衰減計算
					//unity_WorldToLight內置矩陣，世界座標到光源空間變換矩陣。與頂點的世界座標相乘可得到光源空間下的頂點座標
					float3 lightCoord = mul( unity_WorldToLight, float4(i.worldPos, 1)).xyz;
					
					//return float4(lightCoord , 1);
					//實驗後我自己的解讀為 "該頂點位於點光源當中的哪一區域，會根據點光源的旋轉角度改變"

					//利用Unity內置函數tex2D對Unity內置紋理_LightTexture0進行紋理採樣計算光源衰減，獲取其衰減紋理，
					//再通過UNITY_ATTEN_CHANNEL得到衰減紋理中衰減值所在的分量，以得到最終的衰減值
					fixed atten = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL;
				#elif defined (SPOT)   //聚光燈的光照衰減計算
					float4 lightCoord = mul(unity_WorldToLight, float4(i.worldPos, 1));
					
					//return float4(lightCoord , 1);
					//實驗後感覺跟上面的結論差不多，只不過聚光燈照到你時，你基本上一定是在他的Z軸區域

					//(lightCoord.z > 0)：聚光燈的深度值小於等於0時，則光照衰減爲0
					//_LightTextureB0：如果該光源使用了cookie(燈打上去的形狀)，則衰減查找紋理則爲_LightTextureB0
					fixed atten = (lightCoord.z > 0) * tex2D(_LightTexture0, lightCoord.xy / lightCoord.w + 0.5).w * tex2D(_LightTextureB0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL;
				#else
					fixed atten = 1.0;
				#endif
			#endif

					//這裏不再計算環境光，在上個Base Pass中已做計算
					return fixed4((diffuse + specular) * atten, 1.0);
				}
				ENDCG
			}
	}

}
