﻿Shader "Unlit/Fresnel Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_HighlightClr("HighlightClr" , Color) = (1,1,1,1)
		_FresnelPwr("FresnelPower" , float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 normal:V_NORMAL;
				float3 worldPos:WORLD_POS;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _HighlightClr;
			float _FresnelPwr;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//o.normal = v.normal; //原本模型上的法線向量(需要做下面的轉換才會變成世界座標中看到的法線向量)
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				/*o.worldPos = UnityObjectToWorldNormal(v.vertex);*/
				//將自己身上頂點轉換到世界座標 (頂點乘上ObjectToWorld的矩陣)
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				//float3 vertexNormal = i.normal;
				float3 vertexNormal = normalize( i.normal );
				float3 camPos = _WorldSpaceCameraPos;//內建變數，取得Camera的World位置

				float3 viewSrcDir = camPos - i.worldPos; //頂點看向攝影機
				viewSrcDir = normalize(viewSrcDir);
				float fresnelValue = 1 - max(0, dot(viewSrcDir, vertexNormal));

				fresnelValue = pow(fresnelValue, _FresnelPwr);

				float4 fresnelClr = _HighlightClr * fresnelValue;

				return (col *(1 - fresnelValue)) + fresnelClr;

                return col;
            }
            ENDCG
        }
    }
}
