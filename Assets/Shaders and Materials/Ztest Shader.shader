﻿Shader "Unlit/ZTest Shader"
{
	Properties
	{
		_NormalColor("Normal Color", Color) = (1, 1, 1, 1)
		_ZTestColor("ZTest Color", Color) = (1, 1, 1, 1)
		_OutlineColor("Outline Color", Color) = (0, 0, 0, 1)

		//以下測試渲染位置偏移跟在Shader裡面使用Enum
		_OutlineWidth("Outline width", Range(0.0, 1.0)) = .005
		_ZTestOffset("ZTest Offset" , Range(0.0 , 5)) = 0.5
		[KeywordEnum(Offset Left, Offset Right)]_OffsetDir("Offset Dir" , Float) = 0
		[Header(Custom Enum)]
		[Enum(CustomEnum)]  _CustomEnum("CustomEnum", Float) = 1
	}

	SubShader
	{
		Pass //普通渲染
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform float4 _NormalColor; //uniform有點像是把變數宣告成Public (Unity Shader當中預設就已經是公開的樣子)

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 pos : POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				return o;
			}

			float4 frag(v2f i) : COLOR
			{
				return _NormalColor;
			}
			ENDCG
		}

		Pass //渲染外框
		{
			ZTest Greater //比對深度較大的話 (代表視覺上這個頂點在別的頂點背後)

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : POSITION;
			};

			uniform float _OutlineWidth;
			uniform float4 _OutlineColor;
			uniform float _ZTestOffset;
			uniform float _OffsetDir;

			v2f vert(appdata v)
			{
				v2f o;

				float3 norm = normalize(v.normal);
				v.vertex.xyz += v.normal * _OutlineWidth; //將每個頂點的渲染位置往外擴增(依照自己的法線向量)
				//v.vertex.x += (_ZTestOffset * (_OffsetDir * 2 - 1));
				//上面這邊的作法會讓頂點先加X再轉成畫面上的位置，這麼做的話往右移從後方看會變成往左移
				//v.vertex.y += _ZTestOffset; //物體頂點的Y增加會往上

				o.pos = UnityObjectToClipPos(v.vertex);
				//o.pos.x += (_ZTestOffset * (_OffsetDir * 2 - 1));
				//下面這個做法針對轉換出來的畫面位置做偏移，無論從前後看都會往同個方向
				//o.pos.y += _ZTestOffset;//畫面上的Y增加卻是向下
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				return _OutlineColor;
			}
			ENDCG
		}

		Pass //渲染內部(被遮住時變色)
		{
			ZTest Greater

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : POSITION;
			};

			uniform float4 _ZTestColor;
			uniform float _ZTestOffset;
			uniform float _OffsetDir;

			v2f vert(appdata v)
			{
				v2f o;

				float3 norm = normalize(v.normal);
				o.pos = UnityObjectToClipPos(v.vertex);
				//o.pos.x += (_ZTestOffset * (_OffsetDir * 2 - 1));
				//實驗說將顯示結果偏移，發現他會在偏移後才檢查自己是否ZTest Greater(是否有被東西擋住，那東西包含自己)

				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				return _ZTestColor;
			}
			ENDCG
		}
	}
}