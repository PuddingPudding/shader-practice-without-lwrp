﻿Shader "Unlit/Grayscale Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_BrightnessFactor("Brightness Factor", Range(-1.0, 1.0)) = 0
		_GrayscaleFactor("Grayscale Factor", Range(0.0, 1.0)) = 0.1

    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100

        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha
			//常見的混和(Blend)模式，由現在(此頂點的顏色*Alpha值) + (後面其他點顏色*1-Alpha值)算出

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _BrightnessFactor;
			float _GrayscaleFactor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
				//col.rgb = dot(col.rgb, float3(0.3, 0.59, 0.11)); //灰階基本作法
				//float3(0.2125, 0.7154, 0.0721)
				float4 grayScaleClr = float4(dot(col.rgb, float3(0.3, 0.59, 0.11) ).xxx , col.a);
				grayScaleClr.xyz *= (1 + _BrightnessFactor);
				float4 finalClr = lerp(col , grayScaleClr , _GrayscaleFactor);
				return finalClr;
            }
            ENDCG
        }
    }
}
