﻿Shader "Unlit/NewUnlitShader" //Shader的名稱
{
    Properties//Shader的屬性 (主要是讓你在編輯器那邊可以直接手動設定)
    {
        _MainTex ("Texture", 2D) = "white" {}
		//宣告該Shader有著一個2D(一張貼圖)的屬性
		//該屬性在外面顯示為"Texture"，且預設值為白色
    }
    SubShader //子Shader(?)Unity執行時會由上往下找到第一個可用的SubShader
    {//一個Shader可以有好幾個SubShader，但執行時就只會跑第一個可用的
        
		Tags { "RenderType"="Opaque" } //Shader的標籤，這邊RenderType寫Opaque定義了說該Shader為不透明體
        LOD 100 //Level Of Digital，也就是細緻程度，不過我自己調的時候沒啥感覺

        Pass//一次渲染的過程
        {
            CGPROGRAM //宣告從此行開始使用CG語言
            #pragma vertex vert //宣告你的Vertex Shader名稱
            #pragma fragment frag //宣告你的Fragment Shader名稱
            // make fog work
            #pragma multi_compile_fog 

            #include "UnityCG.cginc" //引入函式庫
			#include "Testing.cginc" //試著去引入自己的函式庫
			//#include "test2.cginc" //兩個函式庫都有做#define TESTING_CGINC (若還未define)
			//appdata代表著Unity那邊傳過來的網格資訊例如頂點位置，有時候為了直觀些我會改寫成"VertexInput"
            struct appdata
            {
                float4 vertex : POSITION; //取得頂點的位置
                float2 uv : TEXCOORD0;//取得頂點的uv座標點 (TEXCOORD0為初始的UV座標)
            };
			//v2f代表著Vertex Shader處理完後交給Fragment Shader的資料，有時候為了直觀些我會改寫成"VertexOutput"
            struct v2f
            {
                float2 uv : TEXCOORD0; //等等要交給Fragment Shader的uv座標點
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION; //等等要交給Fragment Shader的頂點渲染位置
            };

            sampler2D _MainTex;//在渲染過程中取得屬性欄裡頭的貼圖
            float4 _MainTex_ST;//這一行比較特別，他會宣告說_MainTex可以讓你進行取樣(SamplerTexture)，但你下面用到的時候只需要打_MainTex
			//vert就是你剛才宣告的Vertex Shader，他會回傳v2f格式的資料
            v2f vert (appdata v)
            {
                v2f o; //宣告要回傳給Fragment Shader的變數
                o.vertex = UnityObjectToClipPos(v.vertex); //將頂點的實際位置轉換成螢幕上看到的位置
                o.uv = TRANSFORM_TEX(v.uv, _MainTex); //取得該頂點對應的UV座標
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
			//frag則是Fragment Shader，接收到v2f資料後上色並回傳顏色資料(fixed4格式，由4個一組的數字構成的資料)
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv); //依據給予的uv座標去跟貼圖(_MainTex)取樣，取出該座標的顏色
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				//return GetTestValue2(); 
				//經測試得知，當你引入兩個cginc檔時，若兩個都有定義A的話，則後面的會被取消，因此引入失敗
				//return GetTestValue(); //試著去呼叫自定義的cginc方法

                return col; //最後該顏色渲染在螢幕上 (渲染在剛剛v2f取得的位置)
            }
            ENDCG//到此行為止，結束CG語言
        }
    }
}
