﻿Shader "Unlit/Camera Try"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_ShakeVolume("Shake Volume" , float) = 1
		_ShakeSpeed("Shake Speed" , float) = 1
		_TwistingSpot("Twisting Spot" , float) = 0.5
		_TwistingLength("Twisting Length", float) = 0.1
		_TwistingWidth("Twistinng Width", float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _ShakeVolume;
			float _ShakeSpeed;
			float _TwistingSpot;
			float _TwistingLength;
			float _TwistingWidth;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				//if (o.vertex.x > 0.25 && o.vertex.x < 0.75)
				//{
					//o.vertex.x += sin(_Time.y * _ShakeSpeed + o.vertex.y * 50) * _ShakeVolume;
				//}
				
				/*if (o.vertex.y >= _TwistingSpot && o.vertex.y <= _TwistingSpot + _TwistingLength)
					o.vertex.x += _TwistingWidth;*/

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				if(o.uv.y >= _TwistingSpot && o.uv.y <= _TwistingSpot + _TwistingLength)
					o.vertex.x += _TwistingWidth;
                //UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
				float4 finalClr = col;
				//finalClr *= (1 + 0.5 * sin(_Time.y));
                return finalClr;
            }
            ENDCG
        }
    }
}
