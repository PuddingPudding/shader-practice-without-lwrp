﻿Shader "Unlit/InvalidSpawn"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color" , Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags {
			"RenderType"="Transparent"
			"Queue" = "Transparent"
		} //照著打到這一段，但是無法實現穿牆渲染，正在思考原因
		
		Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
		ZTest Always
		//好吧，看起來我這邊還得加上ZTest Always，但我看教學的話只有寫ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                //float2 uv : TEXCOORD0;
				float3 normal:NORMAL;
            };

            struct v2f
            {
                //float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 worldPos:WORLD_POS;
				float3 normal:NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(UNITY_MATRIX_M, float4(v.vertex.xyz , 1) );
				//UNITY_MATRIX_M在官方文件上找不到，但他跟unity_ObjectToWorld是一樣的
				//ObjectToWorld相當於該物件在世界座標中的位置，而頂點(vertex)本身的xyz則是該頂點對於此物件來說的相對位置
				//因此我們可以用矩陣相乘的方式去取得該頂點在世界座標當中的具體位置
				
				//o.normal = v.normal;
				o.normal = mul((float3x3)UNITY_MATRIX_M, v.normal);
				//上面的是舊法向量取法，他會取得模型上的法向量 (例如你轉頭180度，眼前的法向量仍然會計為面向Z軸)
				//下面的則是教學中用的新方法，這樣子取到的法向量就會是他在世界座標中的法向量 (這邊轉頭180度，面朝的方向就會變為後方)
				//特別先轉換成3x3矩陣是因為我們只需要用到物體的XYZ向量，不需要最後一行的位置資訊

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				//return float4(i.worldPos , 0.6); //測試每個頂點在世界座標中的位置
				float3 dirToCam = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
				float3 normalTemp = normalize(i.normal); //法向量建議先做單位化，會讓你的結果看起來比較滑順，原因說來話長
				float fresnel = 1 - dot(dirToCam, normalTemp );
				//return float4(fresnel.xxx, 1); //檢測他的fresnel數值
				fresnel = pow(fresnel, 2); //加個次方，讓fresnel效果更銳利
				fresnel = lerp(0.3, 0.8, fresnel); //因為不希望出現完全透明或著太具象(?)的顏色，所以這邊特別拿fresenel值做插值動作，使結果介在0.3~0.8之間

				//return float4 (_Color.xyz , fresnel);
				return float4 (1 , 0 , 0, fresnel);
            }
            ENDCG
        }
    }
}
