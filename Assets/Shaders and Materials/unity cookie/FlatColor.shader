﻿﻿Shader "unityCookie/tut/beginner/1 - Flat Color"
{
	//Interface
	Properties
	{
		_Color("Color", Color) = (1.0,1.0,1.0,1.0)
		_WaveValue("WaveValue",Range(0,1)) = 0.3
		_WaveDensity("WaveDensity",Range(1,30)) = 15
		_TimeCounter("TimeCounter",range(0,6.3)) = 0
	}

		SubShader{
			Pass {
				CGPROGRAM
				//pragmas
				#pragma vertex vert
				#pragma fragment frag

				//user defined variables
				float4 _Color; //原本有加uniform標籤，但Unity當中好像Shader的變數預設都會有
				float _WaveValue;
				float _WaveDensity;
				float _TimeCounter;

	//base input structs
	struct vertexInput
	{
		float4 vertex : POSITION;
	};
	struct vertexOutput
	{
		float4 pos : SV_POSITION;
	};

	//vertex function
	vertexOutput vert(vertexInput v)
	{
		vertexOutput o;
		o.pos = UnityObjectToClipPos(v.vertex);
		//o.pos = UnityObjectToClipPos(v.vertex) + float4(0.1,0,0,0);
		//o.pos = UnityObjectToClipPos(v.vertex + float4(1, 0, 0, 0) );

		o.pos.x = o.pos.x + sin((o.pos.y) * _WaveDensity + _TimeCounter) * _WaveValue;
		return o;
	}

	//fragment function
	float4 frag(vertexOutput i) : COLOR
	{
		return _Color;
	}

	ENDCG
}
	}
		//fallback commented out during development
		//Fallback "Diffuse"
}

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
//照著包子Shader教學打出，Unity會自動把你的mul(UNITY_MATRIX_MVP,*)改換成新的寫法
//特別注意Shader的第一行一定要是Shader的名字，因此我後來把上面的這些註解移到了下面來