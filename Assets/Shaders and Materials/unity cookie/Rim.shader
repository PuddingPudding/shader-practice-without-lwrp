﻿﻿Shader "unityCookie/tut/beginner/4 - Rim Lighting" {
	Properties{
		_Color("Color", Color) = (1.0,1.0,1.0,1.0)
		_SpecColor("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
		_RimColor("Rim Color", Color) = (1.0,1.0,1.0,1.0)
		_RimPower("Rim Power", Range(0.1,10.0)) = 3.0
	}
		SubShader{
			Pass {
				Tags { "LightMode" = "ForwardBase" }
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
		//user defined variables
		uniform float4 _Color;
		uniform float4 _SpecColor;
		uniform float4 _RimColor;
		uniform float _Shininess;
		uniform float _RimPower;
		uniform float4 _LightColor0;
		struct vertexInput {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
		};
		struct vertexOutput {
			float4 pos : SV_POSITION;
			float4 posWorld : TEXCOORD0;
			float3 normalDir : TEXCOORD1;
		};
		//vertex function
		vertexOutput vert(vertexInput v) {
			vertexOutput o;
			o.posWorld = mul(unity_ObjectToWorld, v.vertex);
			o.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
			o.pos = UnityObjectToClipPos(v.vertex);
			return o;
		}
		//fragment function
		float4 frag(vertexOutput i) : COLOR
		{
			float3 normalDirection = i.normalDir;
			float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
			float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			float atten = 1.0;
			//lighting
			float3 diffuseReflection = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection));
			float3 specularReflection = atten * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection)) *
			 pow(saturate(dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
			//Rim Lighting
			float rim = 1 - saturate(dot(normalize(viewDirection), normalDirection));
			//saturate運算，當你帶入的參數<0，那就回傳0，大於1則回傳1，其餘狀況不變
			float3 rimLighting = atten * _LightColor0.xyz * _RimColor * saturate(dot(normalDirection, lightDirection)) *
			pow(rim, _RimPower);
			//這邊的運算結果，越接近邊緣的部分就越亮 (感覺跟Fresnel一樣)

			float3 lightFinal = rimLighting + diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.rgb;

			return float4(lightFinal * _Color.xyz, 1.0);
		}

		ENDCG
	}
	}
		//Fallback "Specular"
}

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
