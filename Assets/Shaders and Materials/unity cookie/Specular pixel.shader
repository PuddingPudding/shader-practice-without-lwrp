﻿﻿Shader "unityCookie/tut/beginner/3b - Specular Pixel" { //從包子投影片上看到的Shader
	Properties{
		_Color("Color", Color) = (1.0,1.0,1.0,1.0)
		_SpecColor("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess("Shininess", Float) = 10
	}
		SubShader{
			Tags { "LightMode" = "ForwardBase" }
			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				//user defined variables
				uniform float4 _Color;
				uniform float4 _SpecColor;
				uniform float _Shininess;

				//unity defined variables;
				uniform float4 _LightColor0;

				//structs
				struct vertexInput {
					float4 vertex : POSITION;
					float3 normal : NORMAL;
				};
				struct vertexOutput {
					float4 pos : SV_POSITION;
					float4 posWorld : TEXCOORD0;
					float3 normalDir : TEXCOORD1;
				};
				//vertex function
				vertexOutput vert(vertexInput v) {
					vertexOutput o;
					o.posWorld = mul(unity_ObjectToWorld, v.vertex);
					/*o.normalDir = normalize(mul(unity_ObjectToWorld ,float4(v.normal, 0.0)).xyz);*/
					o.normalDir = normalize(mul(unity_ObjectToWorld, v.normal) ); //將法向量轉換成世界座標中的法向量
					o.pos = UnityObjectToClipPos(v.vertex);
					return o;

				}
				//fragment function
				float4 frag(vertexOutput i) : COLOR
				{
					//vectors
					float3 normalDirection = i.normalDir;
					float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
					float3 lightDirection;
					float atten = 1.0; //光照強度，平行光永遠視為全滿
					//lighting
					lightDirection = normalize(_WorldSpaceLightPos0.xyz); //光罩方向
					float3 diffuseReflection = atten * _LightColor0.xyz * max(0.0, dot(normalDirection, lightDirection));
					float3 specularReflection = atten * _LightColor0.xyz * _SpecColor.rgb * max(0.0, dot(normalDirection, lightDirection)) *
												pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);

					float3 lightFinal = diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT;
					//UNITY_LIGHTMODEL_AMBIENT可取得目前的環境光
					//return float4(UNITY_LIGHTMODEL_AMBIENT.xyz , 1);
					return float4(lightFinal * _Color.rgb, 1.0);
				}
				ENDCG
			}
	}
}
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
