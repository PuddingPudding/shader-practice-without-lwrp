﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ExplosiveBarrelMgr : MonoBehaviour
{
    public static List<ExplosiveBarrel> AllBarrels = new List<ExplosiveBarrel>();

    public static void UpdateAllBarrelsColor()
    {
        for(int i = 0; i < AllBarrels.Count; i++)
        {
            AllBarrels[i].ApplyColor();
        }
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < AllBarrels.Count; i++)
        {
            //Gizmos.DrawLine(this.transform.position, AllBarrels[i].transform.position);
#if UNITY_EDITOR
            //Handles.DrawAAPolyLine(this.transform.position, AllBarrels[i].transform.position);
            //講者表示比較喜歡下面的這種線，不過要注意，Handles是只有編輯器才能用的
            //因此Handles相關的程式碼都最後特別加個 #if UNITY_EDITOR

            if(AllBarrels[i].data == null) { continue; }

            Vector3 mgrPos = this.transform.position;
            Vector3 barrelPos = AllBarrels[i].transform.position;
            float fHalfHeight = (mgrPos.y - barrelPos.y) * 0.5f;
            Vector3 offset = Vector3.up * fHalfHeight;
            Handles.DrawBezier(mgrPos, barrelPos
                , mgrPos - offset
                , barrelPos + offset
                , AllBarrels[i].data.m_clr
                , EditorGUIUtility.whiteTexture
                , 1); //繪製一個曲線型的線條到每個桶子上
#endif
        }
    }
}
