﻿public enum CustomEnum
{
    Enum1 = 0,
    Enum2 = 1,
    Enum3 = 2
}

public enum EDissipateStyle
{
    CUTOFF = 0,
    DISSOLVE = 1
}