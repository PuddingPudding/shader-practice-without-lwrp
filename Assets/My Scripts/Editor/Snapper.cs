﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class Snapper
{
    public static float RoundByUnit(float _fInput, float _fRoundUnit = 1)
    {
        _fInput /= _fRoundUnit;
        _fInput = Mathf.Round(_fInput);
        _fInput *= _fRoundUnit;
        return _fInput;
    }
    /// <summary>
    /// 對此Vector3進行校正，預設單位為1
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector3 RoundPos(this Vector3 v, float _fRoundUnit = 1)
    {
        v.x = Snapper.RoundByUnit(v.x, _fRoundUnit);
        v.y = Snapper.RoundByUnit(v.y, _fRoundUnit);
        v.z = Snapper.RoundByUnit(v.z, _fRoundUnit);
        return v;
    }// 擴充方法(Extension method)，這邊幫Vector3擴充了一個RoundPos的函式，之後就可以直接打Vector3.RoundPos來進行呼叫
     //(特別注意，擴充方法必須是static類別當中的static函式才行)

    const string UNDO_STR_SNAP = "snap objects"; //用來標記Undo動作的字串

    [MenuItem("Edit/Snap Selected Objects %&s")]//宣告選單功能(讓你在編輯器那邊按Edit->Snap Selected Object)
    public static void SnapTheThings() //你在後面看到的"%&s" 代表著該功能的熱鍵 (Ctrl+Alt+s)
    {
        Debug.Log("Snap the things");
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            GameObject goTemp = Selection.gameObjects[i];
            Undo.RecordObject(goTemp.transform, UNDO_STR_SNAP); //請Undo開始監控你要的資料，若有變動的話它會將其記錄下來
            goTemp.transform.position = goTemp.transform.position.RoundPos();
        }
    }
    [MenuItem("Edit/Snap Selected Objects %&s", isValidateFunction: true)]
    public static bool SnapTheThingsValidate()
    {//判斷說該選項(Snape Selected Objects)能否使用，這裡的條件是去看你有沒有選到物件
        return Selection.gameObjects.Length > 0;
    }

    public static float AtLeast(this float _fValue, float _fMin) { return Mathf.Max(_fValue, _fMin); }
    public static int AtLeast(this int _iValue, int _fMin) { return Mathf.Max(_iValue, _fMin); }

    [MenuItem("Edit/這是一個MenuItem")] //MenuItem標籤，後面則是宣告其路徑
    public static void MenuItemTest()
    {
        Debug.Log("點到MenuItem"); //沒啥幹用的範例，我們先移到下面
    }
}