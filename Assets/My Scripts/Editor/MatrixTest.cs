﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MatrixTest : EditorWindow
{
    [MenuItem("Tools/Matrix Test")]
    public static void OpenWindow() => GetWindow<MatrixTest>();
    //public static void OpenGrimm() => GetWindow<GrimmCannon>(); //實驗，跑去開別人的視窗，好像同類別的視窗依然只能開一個，但可以由別人來開

    [SerializeField]
    private int m_iSpawnCount = 4;
    [SerializeField]
    private float m_fRadius = 3;
    [SerializeField]
    private List<GameObject> m_listPrefabSelected = new List<GameObject>();

    private Matrix4x4 m_matrixPreview;
    //在範圍內隨即生成物件時的探詢點 (從我們看到的方向隨機部屬多個Raycast)
    SpawnData[] m_arrSpawnData;
    /// <summary>
    /// 投影點的基礎距離 (主要用於生成點的投射位置)
    /// </summary>
    private float m_fProjectionOffset = 1f;
    private GameObject[] m_arrPrefabs;
    private bool[] m_bArrPrefabSelection;

    private Material m_invalidMaterial;

    SerializedObject m_serializedObj;
    SerializedProperty m_propSpawnCount;
    SerializedProperty m_propRadius;
    SerializedProperty m_propListSpawnObj;

    private const int m_iCircleDetail = 128;

    private void OnEnable()
    {
        m_serializedObj = new SerializedObject(this);
        m_propSpawnCount = m_serializedObj.FindProperty("m_iSpawnCount");
        m_propRadius = m_serializedObj.FindProperty("m_fRadius");
        m_propListSpawnObj = m_serializedObj.FindProperty("m_listPrefabSelected");

        this.GenerateRandomPoints();

        Shader shTemp = Shader.Find("Unlit/InvalidSpawn");
        m_invalidMaterial = new Material(shTemp);
        //去尋找你要的Shader (直接打路徑名稱)
        //接著依照該Shader生成一個新的材 
        //(但注意，你這樣子會生成一個Asset檔案，因此你需要在關閉視窗後刪除)

        this.m_matrixPreview = Matrix4x4.TRS(new Vector3(1, 2, -10)
                            , Quaternion.Euler(0, 30, 0)
                            , new Vector3(1, 1, 1));
        Debug.Log("測試用矩陣:\n" + this.m_matrixPreview);
        SceneView.onSceneGUIDelegate += this.DuringSceneGUI;

        this.LoadSpawnablePrefabs();
    }
    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= this.DuringSceneGUI;
        DestroyImmediate(m_invalidMaterial);
    }
    private void OnGUI() //編輯器視窗的Update
    {
        m_serializedObj.Update();
        EditorGUILayout.PropertyField(m_propSpawnCount);
        m_propSpawnCount.intValue = m_propSpawnCount.intValue.AtLeast(1);
        EditorGUILayout.PropertyField(m_propRadius);
        m_propRadius.floatValue = m_propRadius.floatValue.AtLeast(1);
        EditorGUILayout.PropertyField(m_propListSpawnObj);
        if (m_serializedObj.ApplyModifiedProperties())
        {
            this.GenerateRandomPoints();
            SceneView.RepaintAll();
        }
    }

    void DuringSceneGUI(SceneView _sceneView) //場景畫面上的Update (編輯模式下每幀呼叫)
    {
        Handles.BeginGUI(); //宣告說從這邊開始，繪製UI在SceneView當中
        Rect rect = new Rect(8, 8, 64, 64);
        for (int i = 0; i < m_arrPrefabs.Length; i++)
        {
            GameObject currentPrefab = m_arrPrefabs[i];
            Texture icon = AssetPreview.GetAssetPreview(m_arrPrefabs[i]);//取得物件的預覽外觀
            EditorGUI.BeginChangeCheck();
            m_bArrPrefabSelection[i] = GUI.Toggle(rect, m_bArrPrefabSelection[i], new GUIContent(icon));
            if (EditorGUI.EndChangeCheck())
            {
                this.RefreshSpawnList();
            }

            rect.y += (rect.height + 2); //繪製畫面上的GUI時，y往下是正
        }
        Handles.EndGUI();

        bool bHoldingAlt = (Event.current.modifiers & EventModifiers.Alt) != 0; //去判斷是否壓著Alt
        //bool bHoldingQ = Event.current.keyCode == KeyCode.Q; //判斷是否壓著Q
        float fScrollDir = Mathf.Sign(Event.current.delta.y);//因為不知道滾輪每次轉動的變化量，所以我們只取它是正是負

        //if (Event.current.type == EventType.ScrollWheel && bHoldingQ)
        //{//原本想調成按下Q鍵時滾輪可以調整生成點數量，但滾輪與普通按鍵的判定好像無法同時觸發
        //    Debug.Log("有進到Q鍵+滾輪定");
        //    m_serializedObj.Update();
        //    m_propSpawnCount.intValue += (fScrollDir > 0) ? 1 : -1;
        //    if (m_serializedObj.ApplyModifiedProperties())
        //    {
        //        this.GenerateRandomPoints();
        //        SceneView.RepaintAll();
        //    }
        //}
        if (Event.current.type == EventType.ScrollWheel && !bHoldingAlt)
        {//每當滑鼠滾輪轉動時，去調整我們的半徑
            m_serializedObj.Update();
            m_propRadius.floatValue *= 1 + (fScrollDir * 0.08f); //讓你在小範圍縮放半徑時，速度變得比較慢，較好掌控
            m_serializedObj.ApplyModifiedProperties();
            //這裡我們需要在每次滾輪移動後順便去修改編輯視窗中的數值 (雖然過幾秒也會改)
            this.Repaint();
            //到這邊為止會發現另一個小問題，當我們滾動轉輪的時後，攝影機會跟著往前/後，因此我們可以使用Use()來蓋過其他的事件
            Event.current.Use();
        }

        Transform camTf = _sceneView.camera.transform;
        if (TryRaycastFromCamera(camTf.up, out Matrix4x4 transformInfo))
        {
            Debug.Log("射中了! 射中之點位資訊:\n" + transformInfo);
            List<SpawnPoint> listSP = this.GetSpawnPoints(transformInfo);

            if (Event.current.type == EventType.Repaint)
            {
                this.DrawCircleRegion(transformInfo);
                this.DrawSpawnPreviews(listSP, _sceneView.camera);
            }
            //取得每個擊中地形的射線點位後，過來檢查是否要生成
            if (/*Event.current.type == EventType.KeyUp*/
                Event.current.type == EventType.KeyDown //教學裡用Down，藉此實現長押連續生成
            && Event.current.keyCode == KeyCode.Space)
            {
                /*試著生成所需物件*/
                this.TrySpawnObjects(listSP);
            }
        }

        #region 測試原matrix的各行各列之向量
        //Vector3 mtxPos = m_matrixPreview.GetColumn(3);
        //Handles.DrawWireDisc(mtxPos , Vector3.up , 0.6f);
        //Handles.color = Color.red;
        //Handles.DrawAAPolyLine(3 , mtxPos, mtxPos + (Vector3)m_matrixPreview.GetRow(0));
        //Handles.color = Color.green;
        //Handles.DrawAAPolyLine(3, mtxPos, mtxPos + (Vector3)m_matrixPreview.GetRow(1));
        //Handles.color = Color.blue;
        //Handles.DrawAAPolyLine(3, mtxPos, mtxPos + (Vector3)m_matrixPreview.GetRow(2));
        //Handles.color = Color.yellow;
        //Handles.DrawAAPolyLine(3, mtxPos, mtxPos + (Vector3)m_matrixPreview.GetColumn(0) );
        //Handles.color = Color.grey;
        //Handles.DrawAAPolyLine(3, mtxPos, mtxPos + (Vector3)m_matrixPreview.GetColumn(1));
        //Handles.color = Color.magenta;
        //Handles.DrawAAPolyLine(3, mtxPos, mtxPos + (Vector3)m_matrixPreview.GetColumn(2));
        #endregion 測試原matrix的各行各列之向量

        if (Event.current.type == EventType.MouseMove)
        {//每次滑鼠移動就通知遊戲場景的GUI更新
            _sceneView.Repaint();
        }
    }

    bool TryRaycastFromCamera(Vector3 cameraUp, out Matrix4x4 tangetToWorldMtx)
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        if (Physics.Raycast(ray , out RaycastHit hit))
        {
            Vector3 hitNormal = hit.normal;
            //Debug.Log("測試Raycast，cameraUp為" + cameraUp);
            Vector3 hitTangent = Vector3.Cross(hitNormal, cameraUp).normalized;
            //射中之點的切線向量，後面的Cross會去回傳一個垂直於兩向量的第三向量
            Vector3 hitBitangent = Vector3.Cross(hitNormal, hitTangent); //取得副切線向量

            Color originClr = Handles.color;
            Handles.color = Color.blue;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitNormal);
            Handles.color = Color.red;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitTangent);
            Handles.color = Color.green;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitBitangent);
            Handles.color = originClr;

            tangetToWorldMtx = Matrix4x4.TRS(hit.point, Quaternion.LookRotation(-hitNormal, hitBitangent), Vector3.one);
            return true;
        }
        else
        {
            tangetToWorldMtx = default;
            return false;
        }
    }

    void GenerateRandomPoints()
    {
        m_arrSpawnData = new SpawnData[m_iSpawnCount];
        for (int i = 0; i < m_iSpawnCount; i++)
        {
            m_arrSpawnData[i].SetRandomValues(m_listPrefabSelected);
        }
    }

    private List<SpawnPoint> GetSpawnPoints(Matrix4x4 _tangentToWorld)
    {
        List<SpawnPoint> listOutput = new List<SpawnPoint>();

        for (int i = 0; i < m_arrSpawnData.Length; i++)
        {
            Vector3 finalPointInDisc = m_arrSpawnData[i].pointInDisc * m_fRadius;

            //finalPointInDisc.z = -m_fProjectionOffset;
            //Matrix4x4 spawnPointLocalMtx = Matrix4x4.TRS(finalPointInDisc
            //    , Quaternion.Euler(0, 0, 0), Vector3.one);
            //Matrix4x4 finalMtx = _tangentToWorld * spawnPointLocalMtx;

            //#region 測試row(2)和column(2)哪一個才是物體的forward向量
            //Handles.color = Color.yellow;
            //Handles.DrawAAPolyLine(7 , finalMtx.GetColumn(3) , finalMtx.GetColumn(3) + finalMtx.GetRow(2));
            //Handles.DrawWireDisc(finalMtx.GetColumn(3), finalMtx.GetRow(2) , 0.4f);
            //Handles.color = Color.blue;
            //Handles.DrawAAPolyLine(7, finalMtx.GetColumn(3), finalMtx.GetColumn(3) + finalMtx.GetColumn(2));
            //Handles.DrawWireDisc(finalMtx.GetColumn(3), finalMtx.GetColumn(2), 0.4f);
            //#endregion 測試row(2)和column(2)哪一個才是物體的forward向量

            //Vector3 rayStart = finalMtx.GetColumn(3);
            //Vector3 rayDir = finalMtx.GetColumn(2);
            //Ray spawnRayTest = new Ray(rayStart, rayDir);

            Ray spawnRayTest = this.GetCircleRay(_tangentToWorld, finalPointInDisc);
            //Handles.color = Color.blue;
            //Handles.DrawAAPolyLine(7, spawnRayTest.origin, spawnRayTest.origin + spawnRayTest.direction);
            //Handles.DrawWireDisc(spawnRayTest.origin, spawnRayTest.direction, 0.4f);
            if (Physics.Raycast(spawnRayTest, out RaycastHit raycastHit)) //試著朝生成點位所看的方向打出射線
            {
                Quaternion rot = Quaternion.LookRotation(raycastHit.normal);
                Quaternion randomRot = Quaternion.Euler(0f, 0f, m_arrSpawnData[i].randAngleDeg);
                rot = rot * (randomRot * Quaternion.Euler(90f, 0f, 0f));
                SpawnPoint spTemp = new SpawnPoint(raycastHit.point, rot, m_arrSpawnData[i]);
                listOutput.Add(spTemp);
            }
        }
        //Handles.color = Color.white;

        return listOutput;
    }
    private void TrySpawnObjects(List<SpawnPoint> _listSP)
    {
        for (int i = 0; i < _listSP.Count; i++)
        {
            if (_listSP[i].isValid) { this.TrySpawnObj(_listSP[i]); }
        }
        //this.GenerateRandomPoints();
        this.RefreshSpawnList(); //刷新欲生成的物件
    }
    private void TrySpawnObj(SpawnPoint _sp)
    {
        if (_sp.Prefab != null)
        {
            GameObject spawnedObj = (GameObject)PrefabUtility.InstantiatePrefab(_sp.Prefab);
            //PrefabUtility.InstantiatePrefab跟平常的GameObject.Instantiate很像
            //只是這個方法叫出來的物件會跟原Prefab有連結 (原Prefab有調動，生成的也會跟著調動)
            Undo.RegisterCreatedObjectUndo(spawnedObj, "物件被生成");
            //向Undo登記一個生成物件的事件
            spawnedObj.transform.position = _sp.position;
            spawnedObj.transform.rotation = _sp.rotation;
        }
    }

    private void DrawCircleRegion(Matrix4x4 _projectingMtx)
    {
        Vector3[] circlePoints = new Vector3[m_iCircleDetail + 1];
        for (int i = 0; i < circlePoints.Length; i++)
        {
            float t = i / (float)m_iCircleDetail;
            const float TAU = Mathf.PI * 2;
            float angRad = t * TAU;
            Vector2 drawPoint = new Vector2(Mathf.Cos(angRad), Mathf.Sin(angRad)) * m_fRadius;
            Ray rayForCicle = this.GetCircleRay(_projectingMtx, drawPoint);
            if (Physics.Raycast(rayForCicle, out RaycastHit cHit))
            {
                circlePoints[i] = cHit.point + cHit.normal * 0.02f;
            }
            else { circlePoints[i] = rayForCicle.origin; }
        }
        Handles.DrawAAPolyLine(circlePoints);
    }
    private void DrawSpawnPreviews(List<SpawnPoint> _listSP, Camera _cam)
    {
        for (int i = 0; i < _listSP.Count; i++)
        {
            Matrix4x4 poseMtx = Matrix4x4.TRS(_listSP[i].position, _listSP[i].rotation, Vector3.one);
            this.DrawPrefab(_listSP[i].spawnData.spawnPrefab, poseMtx, _cam, _listSP[i].isValid);

            //if(_listSP[i].spawnData.spawnPrefab != null && _listSP[i].isValid)
            //{
            //    Matrix4x4 poseMtx = Matrix4x4.TRS(_listSP[i].position, _listSP[i].rotation, Vector3.one);
            //    this.DrawPrefab(_listSP[i].spawnData.spawnPrefab, poseMtx, _cam , _listSP[i].isValid);
            //}
            //else
            //{                
            //    this.DrawSpawnPoint(_listSP[i]);
            //}
        }
    }
    private void DrawPrefab(GameObject prefab, Matrix4x4 poseToWorld, Camera cam , bool _bValid = true)
    {
        if (prefab != null)
        {
            MeshFilter[] filters = prefab.GetComponentsInChildren<MeshFilter>();
            for (int i = 0; i < filters.Length; i++)
            {
                Matrix4x4 localMtx = filters[i].transform.localToWorldMatrix;
                Matrix4x4 finalTransform = poseToWorld * localMtx;
                Mesh mesh = filters[i].sharedMesh;
                Material matTemp = _bValid? filters[i].GetComponent<MeshRenderer>().sharedMaterial : m_invalidMaterial;
                //matTemp.SetPass(0);//SetPass會去告訴現在繪圖的程式該用Shader的哪個Pass去繪製
                ////global settings, Graphics.X command
                //Graphics.DrawMeshNow(mesh, finalTransform);

                #region 用新的方法來繪製預覽物件
                Graphics.DrawMesh(mesh, finalTransform, matTemp, 0, cam); //官方說明DrawMesh繪製出來的東西會受到光影影響
                #endregion 用新的方法來繪製預覽物件
            }
        }
    }
    private void DrawSpawnPointFromDisc(Vector3 _origin , Vector3 _direction)
    {
        Handles.color = Color.yellow;
        Handles.DrawAAPolyLine(7, _origin, _origin + _direction);
        Handles.DrawWireDisc(_origin, _direction, 0.4f);
        Handles.color = Color.white;
    }
    private void DrawSpawnPoint(SpawnPoint _spawnPoint)
    {
        Handles.color = Color.yellow;
        Handles.SphereHandleCap(-1, _spawnPoint.position, Quaternion.identity, 0.1f, EventType.Repaint);
        Handles.DrawAAPolyLine(_spawnPoint.position, _spawnPoint.position + _spawnPoint.Up);
        Handles.color = Color.white;
    }

    private Ray GetCircleRay(Matrix4x4 _tangentToWorld, Vector2 _pointInDisc)
    {
        Vector3 finalPointInDisc = _pointInDisc;
        finalPointInDisc.z = -m_fProjectionOffset;
        Matrix4x4 pointLocalMtx = Matrix4x4.TRS(finalPointInDisc
                , Quaternion.Euler(0, 0, 0), Vector3.one);
        Matrix4x4 finalMtx = _tangentToWorld * pointLocalMtx;

        Ray outputRay = new Ray(finalMtx.GetColumn(3), finalMtx.GetColumn(2));
        return outputRay;
    }

    private void LoadSpawnablePrefabs()
    {
        //載入所有可被生成的遊戲物件
        string[] guids = AssetDatabase.FindAssets("t:prefab", new string[] { "Assets/Prefab" });
        //FindAssets會回傳一個帶有Assets的GlobalUniqueID之陣列
        //t:prefab會找出所有專案中的.prefab檔，後面帶入Assets/Prefab，指定他只去找Prefab資料夾底下的.prefab檔案
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        //IEnumerable表示可被列舉之物，後面guids.Select則是用到Linq語法，Select裡面帶入的是Selector(一個function)
        //Selector會把整個陣列中的元素扔進去並回傳結果
        //以這邊為例的話，他把所有的guid扔進了GUIDToAssetPath，然後將得到的路徑字串回傳給paths變數
        m_arrPrefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
        m_bArrPrefabSelection = new bool[m_arrPrefabs.Length];
        foreach (string sPath in paths)
        {
            Debug.Log(sPath);
        }
    }

    /// <summary>
    /// 刷新自己現在欲生成的物件列表 (目前會順便去刷新隨機生成點)
    /// </summary>
    private void RefreshSpawnList()
    {
        m_listPrefabSelected.Clear();
        for (int i = 0; i < m_arrPrefabs.Length; i++)
        {
            if (m_bArrPrefabSelection[i])
            {
                m_listPrefabSelected.Add(m_arrPrefabs[i]);
            }
        }
        for (int i = 0; i < m_listPrefabSelected.Count; i++)
        {
            Debug.Log(m_listPrefabSelected[i].name);
        }
        this.GenerateRandomPoints();
    }
}