﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// 生成點(擁有投射起點，該點對應之旋轉值的資料)
/// </summary>
public struct SpawnData
{
    public Vector2 pointInDisc;
    public float randAngleDeg;
    public GameObject spawnPrefab;
    public void SetRandomValues(List<GameObject> _listPrefabs = null)
    {
        pointInDisc = UnityEngine.Random.insideUnitCircle;
        randAngleDeg = UnityEngine.Random.value * 360;
        if (_listPrefabs != null && _listPrefabs.Count > 0) { this.spawnPrefab = _listPrefabs[UnityEngine.Random.Range(0, _listPrefabs.Count)]; }
    }
}
/// <summary>
/// 擴充生成點 (額外帶有該點具體來說在哪的資訊)
/// </summary>
public class SpawnPoint
{
    public SpawnData spawnData;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 Up { get { return this.rotation * Vector3.up; } }
    public GameObject Prefab { get { return this.spawnData.spawnPrefab; } }
    public bool isValid;

    public SpawnPoint(Vector3 _pos, Quaternion _rot, SpawnData _data)
    {
        this.spawnData = _data;
        this.position = _pos;
        this.rotation = _rot;

        if (this.Prefab != null)
        {
            SpawnablePrefab prefabTemp = this.Prefab.GetComponent<SpawnablePrefab>();
            if (prefabTemp == null) { this.isValid = true; }
            else
            {
                float h = prefabTemp.height;
                Ray ray = new Ray(this.position, this.Up);
                this.isValid = !Physics.Raycast(ray, out RaycastHit hit);
                //沒有打到東西的話就算是OK，可正常生成
            }
        }
    }
}

public class GrimmCannon : EditorWindow
{
    [MenuItem("Tools/Grimm Cannon")]
    public static void OpenGrimm() => GetWindow<GrimmCannon>();

    [SerializeField]
    private float m_fRadius = 2f;
    [SerializeField]
    private int m_iSpawnCount = 8;
    //[SerializeField]
    //private GameObject m_spawnPrefab = null;
    [SerializeField]
    private List<GameObject> m_listSpawnPrefab = new List<GameObject>();
    [SerializeField]
    private bool[] m_bArrPrefabSelection;

    private GameObject[] m_arrPrefabs;

    //在範圍內隨即生成物件時的探詢點 (從我們看到的方向隨機部屬多個Raycast)
    SpawnData[] m_listSpawnDataPoints;

    SerializedObject m_serializedObj;
    SerializedProperty m_propRadius;
    SerializedProperty m_propSpawnCount;
    SerializedProperty m_propSpawnPrefab;
    SerializedProperty m_propPreviewMatreial;

    private void OnEnable()
    {
        m_serializedObj = new SerializedObject(this);
        m_propRadius = m_serializedObj.FindProperty("m_fRadius");
        m_propSpawnCount = m_serializedObj.FindProperty("m_iSpawnCount");
        m_propSpawnPrefab = m_serializedObj.FindProperty("m_spawnPrefab");
        m_propSpawnPrefab = m_serializedObj.FindProperty("m_listSpawnPrefab");
        m_propPreviewMatreial = m_serializedObj.FindProperty("m_previewMaterial");
        this.GenerateRandomPoints();
        SceneView.onSceneGUIDelegate += this.DuringSceneGUI;

        //載入所有可被生成的遊戲物件
        string[] guids = AssetDatabase.FindAssets("t:prefab", new string[] { "Assets/Prefab" });
        //FindAssets會回傳一個帶有Assets的GlobalUniqueID之陣列
        //t:prefab會找出所有專案中的.prefab檔，後面帶入Assets/Prefab，指定他只去找Prefab資料夾底下的.prefab檔案
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        //IEnumerable表示可被列舉之物，後面guids.Select則是用到Linq語法，Select裡面帶入的是Selector(一個function)
        //Selector會把整個陣列中的元素扔進去並回傳結果
        //以這邊為例的話，他把所有的guid扔進了GUIDToAssetPath，然後將得到的路徑字串回傳給paths變數
        m_arrPrefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();

        foreach (string sPath in paths)
        {
            Debug.Log(sPath);
        }
        //for (int i = 0; i < paths.Count(); i++) { Debug.Log(paths.ElementAt(i)); }

        if (m_bArrPrefabSelection == null
            || m_bArrPrefabSelection.Length != m_arrPrefabs.Length)
        {
            m_bArrPrefabSelection = new bool[m_arrPrefabs.Length];
        }
    }
    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= this.DuringSceneGUI;
    }

    void TrySpawnObjects(List<SpawnPoint> _listSpawnPoints)
    {
        if (m_listSpawnPrefab == null) { return; }
        for (int i = 0; i < _listSpawnPoints.Count; i++)
        {
            GameObject spawnedThing = (GameObject)PrefabUtility.InstantiatePrefab(m_listSpawnPrefab[0]);
            Undo.RegisterCompleteObjectUndo(spawnedThing, "Spawn Objects");
            //向Undo註冊物件生成事件
            spawnedThing.transform.position = _listSpawnPoints[i].position;
            spawnedThing.transform.rotation = _listSpawnPoints[i].rotation;
        }
        this.GenerateRandomPoints(); //更新生成點
    }

    void GenerateRandomPoints()
    {
        m_listSpawnDataPoints = new SpawnData[m_iSpawnCount];
        for (int i = 0; i < m_iSpawnCount; i++)
        {
            m_listSpawnDataPoints[i].SetRandomValues(this.m_listSpawnPrefab);
        }
    }

    bool TryRaycastFromCamera(Vector3 cameraUp, out Matrix4x4 tangetToWorldMtx)
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Vector3 hitNormal = hit.normal;
            //Debug.Log("測試Raycast，cameraUp為" + cameraUp);
            Vector3 hitTangent = Vector3.Cross(hitNormal, cameraUp).normalized;
            //射中之點的切線向量，後面的Cross會去回傳一個垂直於兩向量的第三向量
            Vector3 hitBitangent = Vector3.Cross(hitNormal, hitTangent); //取得副切線向量
            //Debug.Log("依序顯示Normal,Tangent,Bitangent\n"
            //    + hitNormal + "\n"
            //    + hitTangent + "\n"
            //    + hitBitangent);
            tangetToWorldMtx = Matrix4x4.TRS(hit.point, Quaternion.LookRotation(hitNormal, hitBitangent), Vector3.one);
            return true;
        }
        else
        {
            tangetToWorldMtx = default;
            return false;
        }
    }

    private void OnGUI() //編輯器視窗的Update
    {
        m_serializedObj.Update();
        EditorGUILayout.PropertyField(m_propRadius);
        m_propRadius.floatValue = m_propRadius.floatValue.AtLeast(1); //用instance method去設定說最小值為1
        EditorGUILayout.PropertyField(m_propSpawnCount);
        m_propSpawnCount.intValue = m_propSpawnCount.intValue.AtLeast(1);
        EditorGUILayout.PropertyField(m_propSpawnPrefab); //在你的編輯器新增GameObject的欄位
        //不過你無法在這行指令裡面要求說他不能參照場景中的物件
        if (m_serializedObj.ApplyModifiedProperties())
        {
            this.GenerateRandomPoints();
            SceneView.RepaintAll();
            //每當我們有參數改動的時候，要求SceneView立刻重新繪製
        }
        //如果你在編輯器中按滑鼠左鍵，則會觸發
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        { //會加入這一塊是因為你平常在Editor當中要是點其他部分，Editor仍然會維持它原本聚焦的欄位，不會解除
            GUI.FocusControl(null); //這邊加說我們點了左鍵就會解除聚焦
            Repaint();
            //但是有特例，當你左鍵點到屬性欄位時，它依然會正常聚焦，那是因為Unity當中的GUI事件會有吞噬(consume)的狀況，有的事件會蓋過其他的事件
        }
    }

    void DrawSphere(Vector3 _pos) //繪製球體
    {
        Handles.SphereHandleCap(-1, _pos, Quaternion.identity, 0.1f, EventType.Repaint);
        //設定說每次重新繪製時跟著更新
    }

    void TryToSpawn(List<RaycastHit> _hitPts)
    {
        if (m_listSpawnPrefab[0] == null || m_listSpawnPrefab.Count <= 0) { return; }
        for (int i = 0; i < _hitPts.Count; i++)
        {
            Quaternion rot = Quaternion.LookRotation(_hitPts[i].normal);
            GameObject spawnedObj = (GameObject)PrefabUtility.InstantiatePrefab(m_listSpawnPrefab[0]);
            //PrefabUtility.InstantiatePrefab跟平常的GameObject.Instantiate很像
            //只是這個方法叫出來的物件會跟原Prefab有連結 (原Prefab有調動，生成的也會跟著調動)
            Undo.RegisterCreatedObjectUndo(spawnedObj, "物件被生成");
            //向Undo登記一個生成物件的事件
            spawnedObj.transform.position = _hitPts[i].point;

            float fRandomAngle = UnityEngine.Random.value * 360f;
            Quaternion randomRot = Quaternion.Euler(0f, 0f, fRandomAngle);

            rot = rot * (randomRot * Quaternion.Euler(90f, 0f, 0f));
            //先前先把旋轉角度設為面向所打之點的法向量，現在我們要再將其X軸旋轉90度
            //特別注意先後順序是有差的
            spawnedObj.transform.rotation = rot;

            //Instantiate(m_listSpawnPrefab[0], _hitPts[i].point , rot );
        }
        this.GenerateRandomPoints();
    }
    void TryToSpawn(List<Pose> _hitPts)
    {
        if (m_listSpawnPrefab[0] == null || m_listSpawnPrefab.Count <= 0) { return; }
        for (int i = 0; i < _hitPts.Count; i++)
        {
            GameObject spawnedObj = (GameObject)PrefabUtility.InstantiatePrefab(m_listSpawnPrefab[0]);
            //PrefabUtility.InstantiatePrefab跟平常的GameObject.Instantiate很像
            //只是這個方法叫出來的物件會跟原Prefab有連結 (原Prefab有調動，生成的也會跟著調動)
            Undo.RegisterCreatedObjectUndo(spawnedObj, "物件被生成");
            //向Undo登記一個生成物件的事件
            spawnedObj.transform.position = _hitPts[i].position;
            spawnedObj.transform.rotation = _hitPts[i].rotation;
        }
        this.GenerateRandomPoints();
    }

    void DuringSceneGUI(SceneView _sceneView) //畫面上的Update (編輯模式下每幀呼叫)
    {
        Handles.BeginGUI(); //宣告說從這邊開始，繪製UI在SceneView當中
        Rect rect = new Rect(8, 8, 64, 64);
        for (int i = 0; i < m_arrPrefabs.Length; i++)
        {
            GameObject currentPrefab = m_arrPrefabs[i];
            Texture icon = AssetPreview.GetAssetPreview(m_arrPrefabs[i]);//取得物件的預覽外觀
            EditorGUI.BeginChangeCheck();
            m_bArrPrefabSelection[i] = GUI.Toggle(rect, m_bArrPrefabSelection[i], new GUIContent(icon));
            if (EditorGUI.EndChangeCheck())
            {
                m_listSpawnPrefab.Clear();
                for (int j = 0; j < m_arrPrefabs.Length; j++)
                {
                    if (m_bArrPrefabSelection[j]) { m_listSpawnPrefab.Add(m_arrPrefabs[j]); }
                }
            }
            //if (GUI.Toggle(rect, m_listSpawnPrefab[0] == m_arrPrefabs[i], new GUIContent(icon)))
            //{ //若該Toggle對應的物件剛好是現在欲生成的物件，則將其勾選
            //    m_listSpawnPrefab[0] = m_arrPrefabs[i];
            //}

            rect.y += (rect.height + 2); //繪製畫面上的GUI時，y往下是正
        }
        Handles.EndGUI();

        Transform camTf = _sceneView.camera.transform;
        //Ray ray = new Ray(camTf.position, camTf.forward); //從SceneView的畫面中央(Camera)打出一道射線
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        //將GUI元件的位置轉換成世界座標，這裡帶入滑鼠位置

        if (Event.current.type == EventType.MouseMove)
        {//每次滑鼠移動就通知遊戲場景的GUI更新
            _sceneView.Repaint();
        }

        bool bHoldingAlt = (Event.current.modifiers & EventModifiers.Alt) != 0; //去判斷是否壓著Alt

        if (Event.current.type == EventType.ScrollWheel && !bHoldingAlt)
        {//每當滑鼠滾輪轉動時，去調整我們的半徑
            float scrollDir = Mathf.Sign(Event.current.delta.y);//因為不知道滾輪每次轉動的變化量，所以我們只取它是正是負
            m_serializedObj.Update();
            m_propRadius.floatValue *= 1 + (scrollDir * 0.08f); //讓你在小範圍縮放半徑時，速度變得比較慢，較好掌控
            m_serializedObj.ApplyModifiedProperties();
            //這裡我們需要在每次滾輪移動後順便去修改編輯視窗中的數值 (雖然過幾秒也會改)
            this.Repaint();
            //到這邊為止會發現另一個小問題，當我們滾動轉輪的時後，攝影機會跟著往前/後，因此我們可以使用Use()來蓋過其他的事件
            Event.current.Use();
        }

        #region 開發工具教學4的新寫法
        //if (TryRaycastFromCamera(camTf.up, out Matrix4x4 tangentToWorld))
        //{
        //    //List<Pose> spawnPoses = GetSpawnPoses(tangentToWorld); //Pose在Unity當中是只存下位置與旋轉角度的類別
        //    Debug.Log("TangentToWorld矩陣:\n" + tangentToWorld);
        //    List<SpawnPoint> hitPointsNew = this.GetSpawnPoints(tangentToWorld);
        //    foreach (SpawnPoint point in hitPointsNew)
        //    {
        //        Handles.color = Color.yellow;
        //        Handles.DrawWireDisc(point.position, tangentToWorld.GetRow(2), 0.5f);
        //        Handles.color = Color.white;
        //    }
        //} //我實在是看不懂，因此決定暫時作罷
        #endregion 開發工具教學4的新寫法

        //2020/05/19 下方程式碼準備要修改，用新的TryRacastFromCamera
        if (Physics.Raycast(ray, out RaycastHit hit)) //下面是將所射中之物(依照collider判定)的表面法向量顯示出來
        {
            Vector3 hitNormal = hit.normal;
            Vector3 hitTangent = Vector3.Cross(hitNormal, camTf.up).normalized;
            //射中之點的切線向量，後面的Cross會去回傳一個垂直於兩向量的第三向量
            Vector3 hitBitangent = Vector3.Cross(hitNormal, hitTangent);
            //取得副切線向量

            Ray GetTangentRay(Vector2 tangentSpace)
            {
                Vector3 rayOrigin = hit.point + (hitTangent * tangentSpace.x + hitBitangent * tangentSpace.y) * m_fRadius;
                rayOrigin += hitNormal * 2;
                Vector3 rayDir = -hitNormal;
                return new Ray(rayOrigin, rayDir);
            }

            List<Pose> hitPoses = new List<Pose>(); //後來改用Pose資訊來生成物件 (只裝位置和旋轉角度)
            //記住每個射線擊中的資訊
            for (int i = 0; i < m_listSpawnDataPoints.Length; i++)
            {
                Ray pointRay = GetTangentRay(m_listSpawnDataPoints[i].pointInDisc);
                if (Physics.Raycast(pointRay, out RaycastHit raycastHit))
                {
                    Quaternion rot = Quaternion.LookRotation(raycastHit.normal);
                    Quaternion randomRot = Quaternion.Euler(0f, 0f, m_listSpawnDataPoints[i].randAngleDeg);
                    rot = rot * (randomRot * Quaternion.Euler(90f, 0f, 0f));
                    Pose poseTemp = new Pose(raycastHit.point, rot);
                    hitPoses.Add(poseTemp);

                    this.DrawSphere(raycastHit.point);
                    Handles.DrawAAPolyLine(raycastHit.point, raycastHit.point + raycastHit.normal);

                    if (/*m_listSpawnPrefab[0] != null*/ m_listSpawnPrefab.Count > 0)
                    {
                        #region 新的物件預覽，針對子物件的transform資料做矩陣轉置
                        MeshFilter[] filters = m_listSpawnPrefab[0].GetComponentsInChildren<MeshFilter>();
                        Matrix4x4 poseMtx = Matrix4x4.TRS(poseTemp.position, poseTemp.rotation, Vector3.one);
                        //TRS代表位置，旋轉和大小比例，呼叫此方法來生成一個對應的4*4矩陣
                        //Debug.Log("四乘四矩陣:\n" + poseMtx);
                        for (int j = 0; j < filters.Length; j++)
                        {
                            Matrix4x4 localMtx = filters[j].transform.localToWorldMatrix;
                            //Debug.Log("localToWorldMatrix:\n" + localMtx);
                            //Matrix4x4 worldMtx = filters[j].transform.worldToLocalMatrix;
                            //Debug.Log("worldToLocalMatrix:\n" + worldMtx);
                            Matrix4x4 finalTransform = poseMtx * localMtx;

                            Mesh mesh = filters[j].sharedMesh;
                            Material matTemp = filters[j].GetComponent<MeshRenderer>().sharedMaterial;
                            matTemp.SetPass(0);//SetPass會去告訴現在繪圖的程式該用Shader的哪個Pass去繪製
                            Graphics.DrawMeshNow(mesh, finalTransform);
                        }
                        #endregion 新的物件預覽，針對子物件的transform資料做矩陣轉置

                        #region 原本的物件預覽，只能繪製單一網格的物體
                        ////取得欲生成物件的網格
                        //Mesh mesh = m_spawnPrefab.GetComponent<MeshFilter>().sharedMesh;
                        ////用shardMesh來取得Assets資料夾中物件對應的網格，直接取mesh的話，Unity會額外再生成一個給你
                        //Material matTemp = m_spawnPrefab.GetComponent<MeshRenderer>().sharedMaterial;
                        //matTemp.SetPass(0);//SetPass會去告訴現在繪圖的程式該用Shader的哪個Pass去繪製
                        //Graphics.DrawMeshNow(mesh, poseTemp.position , poseTemp.rotation);
                        ////繪製取得的網格 (記得外面要把物件換成根部帶有MeshFilter的物件)
                        ////據說在Camera沒看到的時候也會進行繪製，所以要特別注意 (並且他只會當下繪製，不會計算任何光影)
                        #endregion 原本的物件預覽，只能繪製單一網格的物體
                        //Debug.Log("取得的網格數量: " + filters.Length);
                    }
                }
            }

            //取得每個擊中地形的射線點位後，過來檢查是否要生成
            if (/*Event.current.type == EventType.KeyUp*/
                Event.current.type == EventType.KeyDown //教學裡用Down，藉此實現長押連續生成
            && Event.current.keyCode == KeyCode.Space)
            {
                //Debug.Log("生成! 事件類別為:" + Event.current.type); //檢測當前發生的事件，GUI的KeyDown是你按著按鈕就會一直觸發
                this.TryToSpawn(hitPoses);
            }

            Color originClr = Handles.color;
            Handles.color = Color.blue;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitNormal);
            Handles.color = Color.red;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitTangent);
            Handles.color = Color.green;
            Handles.DrawAAPolyLine(7, hit.point, hit.point + hitBitangent);
            Handles.color = originClr;

            //根據地形更改圓圈 (不要只是僵硬地將圓圈繪製出來)
            const int iCircleDetail = 128;
            Vector3[] circlePoints = new Vector3[iCircleDetail + 1];
            for (int i = 0; i <= iCircleDetail; i++)
            {
                float t = i / (float)iCircleDetail;
                const float TAU = Mathf.PI * 2;
                float angRad = t * TAU;
                Vector2 dir = new Vector2(Mathf.Cos(angRad), Mathf.Sin(angRad));
                Ray rayForCicle = GetTangentRay(dir);
                if (Physics.Raycast(rayForCicle, out RaycastHit cHit))
                {
                    circlePoints[i] = cHit.point + cHit.normal * 0.02f;
                }
                else
                {
                    circlePoints[i] = ray.origin;
                }
            }
            Handles.DrawAAPolyLine(circlePoints);
            //Handles.DrawWireDisc(hit.point, hitNormal, this.m_fRadius);
        }
    }

    void DrawSpawnPreviews(List<SpawnPoint> _spawnPoints, Camera cam)
    {
        for (int i = 0; i < _spawnPoints.Count; i++)
        {
            //if (m_listSpawnPrefab != null && m_listSpawnPrefab.Count > 0)
            if (_spawnPoints[i].spawnData.spawnPrefab != null)
            {
                Matrix4x4 poseToWorld = Matrix4x4.TRS(_spawnPoints[i].position, _spawnPoints[i].rotation, Vector3.one);
                DrawPrefab(_spawnPoints[i].spawnData.spawnPrefab, poseToWorld, cam);
            }
            else
            {
                Handles.SphereHandleCap(-1, _spawnPoints[i].position, Quaternion.identity, 0.1f, EventType.Repaint);
                Handles.DrawAAPolyLine(_spawnPoints[i].position, _spawnPoints[i].position + _spawnPoints[i].Up);
            }
        }
    }
    static void DrawPrefab(GameObject prefab, Matrix4x4 poseToWorld, Camera cam)
    {
        MeshFilter[] filters = prefab.GetComponentsInChildren<MeshFilter>();
        //TRS代表位置，旋轉和大小比例，呼叫此方法來生成一個對應的4*4矩陣
        //Debug.Log("四乘四矩陣:\n" + poseMtx);
        for (int j = 0; j < filters.Length; j++)
        {
            Matrix4x4 localMtx = filters[j].transform.localToWorldMatrix;
            //Debug.Log("localToWorldMatrix:\n" + localMtx);
            //Matrix4x4 worldMtx = filters[j].transform.worldToLocalMatrix;
            //Debug.Log("worldToLocalMatrix:\n" + worldMtx);
            Matrix4x4 finalTransform = poseToWorld * localMtx;

            Mesh mesh = filters[j].sharedMesh;
            Material matTemp = filters[j].GetComponent<MeshRenderer>().sharedMaterial;
            matTemp.SetPass(0);//SetPass會去告訴現在繪圖的程式該用Shader的哪個Pass去繪製
            Graphics.DrawMeshNow(mesh, finalTransform);
        }
    }

    private Ray GetCircleRay(Matrix4x4 _tangentToWorld, Vector2 _pointInDisc)
    {
        Vector3 originPos = _tangentToWorld.GetColumn(3); //原位置
        Quaternion originRot = Quaternion.LookRotation(_tangentToWorld.GetColumn(2), _tangentToWorld.GetColumn(1)); //取得原本的旋轉值
        Matrix4x4 localMtx = Matrix4x4.TRS(_pointInDisc, originRot, Vector3.one);

        Ray outputRay = new Ray(localMtx.GetColumn(3), localMtx.GetRow(2));
        return outputRay;
    }

    private List<SpawnPoint> GetSpawnPoints(Matrix4x4 tangentToWorld)
    {
        List<SpawnPoint> hitSpawnPoints = new List<SpawnPoint>();
        for (int i = 0; i < m_listSpawnDataPoints.Count(); i++)
        {
            Ray ptRay = GetCircleRay(tangentToWorld, m_listSpawnDataPoints[i].pointInDisc);
            if (Physics.Raycast(ptRay, out RaycastHit ptHit))
            {
                Quaternion randRot = Quaternion.Euler(0, 0, m_listSpawnDataPoints[i].randAngleDeg);
                Quaternion rot = Quaternion.LookRotation(ptHit.normal) * (randRot * Quaternion.Euler(90, 0, 0));
                SpawnPoint spawnPoint = new SpawnPoint(ptHit.point, rot, m_listSpawnDataPoints[i]);
                hitSpawnPoints.Add(spawnPoint);
            }
        }
        return hitSpawnPoints;
    }
    Ray GetTangentRay(Vector2 tangentSpace, Vector3 _hitPos, Vector3 hitNormal, Vector3 hitTangent, Vector3 hitBitangent)
    {
        Vector3 rayOrigin = _hitPos + (hitTangent * tangentSpace.x + hitBitangent * tangentSpace.y) * m_fRadius;
        rayOrigin += hitNormal * 2;
        Vector3 rayDir = -hitNormal;
        return new Ray(rayOrigin, rayDir);
    }
}