﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CanEditMultipleObjects] //宣告說此自製Editor可進行多重編輯，不過你在屬性欄位那邊也會需要做些調整
[CustomEditor(typeof(BarrelData))] //表明說這個自製Editor是給哪個類別用的
public class BarrelDataEditor : Editor
{
    //Things things;
    //float someValue; //先前測試用的

    #region 處理序列化資料 (主要用於記錄變更與否)
    SerializedObject so;
    SerializedProperty propRadius;
    SerializedProperty propDamage;
    SerializedProperty propColor;
    #endregion 處理序列化資料 (主要用於記錄變更與否)

    private void OnEnable() //Editor的OnEnable會在原本Class被選取的時候呼叫
    {
        Debug.Log("桶子資料已被選取");
        so = serializedObject; //打serializedObject會取得我們正要管理的物件
        propRadius = so.FindProperty("m_fRadius");
        propDamage = so.FindProperty("m_fDmg");
        propColor = so.FindProperty("m_clr");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        so.Update(); //初次要顯示屬性欄位的時候需要先做Update
        EditorGUILayout.PropertyField(propRadius);
        EditorGUILayout.PropertyField(propDamage);
        EditorGUILayout.PropertyField(propColor);
        if (so.ApplyModifiedProperties()) //將屬性變化附加回去 (會寫入Undo紀錄)
        { //當你有做變化時，它會回傳true，我們可以利用這一點做到讓你修改ScriptableObject的屬性時，同時去修改原本的屬性
            ExplosiveBarrelMgr.UpdateAllBarrelsColor();
        }


        #region 舊有的變數調整
        //BarrelData barrel = (target as BarrelData); //將該Editor負責處理的類別取出
        //float fNewRaidus = EditorGUILayout.FloatField("半徑", barrel.m_fRadius);
        //if (fNewRaidus != barrel.m_fRadius) //改為將新資料存在暫存變數
        //{//若偵測到改變，則向Undo系統(你平常按Ctrl+z)註冊一個改變事件
        //    Undo.RecordObject(barrel, "修改了爆炸半徑");
        //    barrel.m_fRadius = fNewRaidus;
        //}//不過還有別的方法可以做到Undo
        //barrel.m_fRadius =
        //    EditorGUILayout.FloatField("半徑", barrel.m_fRadius);
        //barrel.m_fDmg =
        //    EditorGUILayout.FloatField("傷害", barrel.m_fDmg);
        //barrel.m_clr =
        //    EditorGUILayout.ColorField("桶子顏色", barrel.m_clr);
        #endregion 舊有的變數調整

        #region 初嘗試加入的一些UI
        //GUILayout.BeginHorizontal();
        ////將Begin到EndHorizontal內的元素包在同一個水平面上
        //GUILayout.EndHorizontal();
        //using (new GUILayout.HorizontalScope())
        //{//後來比較建議的做法，避免你忘了EndHorizontal
        //    if (GUILayout.Button("Do a thing")) { Debug.Log("did the thing."); }
        //    //在編輯器畫面中定義一個按鈕，寫著Do a thing，按下去後會顯示Log訊息
        //    things = (Things)EditorGUILayout.EnumPopup(things);
        //    //宣告一個enum的下拉式選單
        //}
        //GUILayout.Label("Things");
        //GUILayout.Label("Things look like btn", GUI.skin.button);
        //GUILayout.Space(40);
        //GUILayout.Label("Category", EditorStyles.boldLabel);
        //EditorGUILayout.ObjectField("放進來", null, typeof(Transform), true);
        //宣告說我們要一個Transform欄位
        //someValue = GUILayout.HorizontalSlider(someValue , -1f, 1f); //宣告一個拉條(?)給someValue
        #endregion 初嘗試加入的一些UI
    }
}

enum Things { A,B,C}
