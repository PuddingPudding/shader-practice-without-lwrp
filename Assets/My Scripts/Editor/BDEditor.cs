﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(BarrelData))] //刻意宣告另一個重複的處理Class
public class BDEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Label("New editor?"); //測試結果，當出現多個處理同Class的Editor時，Unity只會選擇其中一個
    }
}
