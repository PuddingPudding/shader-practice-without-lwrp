﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class SnapperTool : EditorWindow //試製編輯器視窗
{
    public enum EGridType
    {
        CARTESIAN, //笛卡爾座標 (方格狀的那種)
        POLAR //極座標 (圓圈狀)
    }

    [Range(0.5f, 5f)]
    public float SnapUnit = 0.5f;
    public EGridType Type;
    public int AngularDivisions = 24; //極座標當中，將圓圈切成幾等分

    #region 處理序列化資料 (主要用於記錄變更與否)
    private SerializedObject m_serializedObj;
    private SerializedProperty m_propSnapUnit;
    private SerializedProperty m_propGridType;
    private SerializedProperty m_propAngularDivision;
    #endregion 處理序列化資料 (主要用於記錄變更與否)

    [MenuItem("Tools / Snapper")]
    public static void OpenTheThing() => GetWindow<SnapperTool>("史奈普");
    //上面這裡讓你在編輯器中新增Tools->Snapper的選項路徑
    //按下去後會打開一個標題為"史奈普"的視窗，若已開啟則會跳到視窗內

    //註冊selectionChanged聆聽事件 (每當你選的物件改變時觸發)
    //Repaint則是GUI本來就有的重新繪製function
    private void OnEnable()
    {
        m_serializedObj = new SerializedObject(this); //EditorWindow當中，沒有負責管理之物件的話是不能直接這樣呼叫的
        m_propSnapUnit = m_serializedObj.FindProperty("SnapUnit");
        m_propGridType = m_serializedObj.FindProperty("Type");
        m_propAngularDivision = m_serializedObj.FindProperty("AngularDivisions");

        //載入儲存的設定 (後面為預設值)
        this.SnapUnit = EditorPrefs.GetFloat("SNAPPER_TOOL_snapUnit", 1);
        this.Type = (EGridType)EditorPrefs.GetInt("SNAPPER_TOOL_gridType", 0);
        this.AngularDivisions = EditorPrefs.GetInt("SNAPPER_TOOL_angularDivisions", 24);

        Selection.selectionChanged += Repaint;
        SceneView.onSceneGUIDelegate += this.DuringSceneGUI;
    }
    private void OnDisable()
    {
        //儲存設定值
        EditorPrefs.SetFloat("SNAPPER_TOOL_snapUnit", this.SnapUnit);
        EditorPrefs.SetInt("SNAPPER_TOOL_gridType", (int)this.Type);
        EditorPrefs.SetInt("SNAPPER_TOOL_angularDivisions", this.AngularDivisions);

        Selection.selectionChanged -= Repaint;
        SceneView.onSceneGUIDelegate -= this.DuringSceneGUI;
    }
    //這兩行主要是讓GUI更快的去做反應，否則平時你通常要等一陣子，或手動點到視窗裡才會更新

    private void OnGUI()
    {
        //this.SnapUnit = EditorGUILayout.FloatField("校正單位", this.SnapUnit); //無法根據範圍調整的float值
        m_serializedObj.Update();
        EditorGUILayout.PropertyField(m_propGridType);
        EditorGUILayout.PropertyField(m_propSnapUnit);
        if (this.Type == EGridType.POLAR)
        {
            EditorGUILayout.PropertyField(m_propAngularDivision);
            if (m_propAngularDivision.intValue < 4) { m_propAngularDivision.intValue = 4; }
        }
        m_serializedObj.ApplyModifiedProperties();
        //if(m_serializedObj.ApplyModifiedProperties()) //校正單位的Slider無法因為這段判定而即時更新
        //{
        //    this.Repaint();
        //}

        using (new EditorGUI.DisabledScope(Selection.gameObjects.Length <= 0))
        {//加入disabledScope，用以檢測說這個UI是否不能用
            if (GUILayout.Button("Snap Selection"))
            {
                SnapSelections();
            }
        }
    }

    void DuringSceneGUI(SceneView _sv)
    {
        if (Event.current.type == EventType.Repaint)
        {
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual; //改為只有在線條沒有被擋住時才繪製

            const float gridDrawExtent = 16; //繪製格紋的極限
            if (this.Type == EGridType.CARTESIAN) { this.DrawCartesianGrid(gridDrawExtent); }
            else { this.DrawPolarGrid(gridDrawExtent); }
        }

        #region 官方的在畫面上畫方塊教學
        //// Constrain all drawing to be within a 800x600 pixel area centered on the screen.
        //GUI.BeginGroup(new Rect(Screen.width / 2 - 400, Screen.height / 2 - 300, 800, 600));

        //// Draw a box in the new coordinate space defined by the BeginGroup.
        //// Notice how (0,0) has now been moved on-screen
        //GUI.Box(new Rect(0, 0, 80, 60), "This box is now centered! - here you would put your main menu");

        //// We need to match all BeginGroup calls with an EndGroup
        //GUI.EndGroup();
        #endregion 官方的在畫面上畫方塊教學
    }

    /// <summary>
    /// 依照笛卡爾座標(方形)去繪製校正點
    /// </summary>
    /// <param name="_fDrawExtent"></param>
    private void DrawCartesianGrid(float _fDrawExtent)
    {
        int iLineCount = Mathf.RoundToInt((_fDrawExtent * 2) / SnapUnit);
        if (iLineCount % 2 == 0) { iLineCount++; }

        for (int i = 0; i < iLineCount; i++)
        {
            float xPoint = 0 - ((iLineCount / 2) * SnapUnit) + (SnapUnit * i);
            float zStart = 0 - _fDrawExtent;
            float zEnd = 0 + _fDrawExtent;
            Vector3 startPoint = new Vector3(xPoint, 0, zStart);
            Vector3 endPoint = new Vector3(xPoint, 0, zEnd);
            Handles.DrawPolyLine(startPoint, endPoint);
            startPoint = new Vector3(zStart, 0, xPoint);
            endPoint = new Vector3(zEnd, 0, xPoint);
            Handles.DrawPolyLine(startPoint, endPoint);
        }
    }
    /// <summary>
    /// 依照極座標(圓圈)去繪製校正點
    /// </summary>
    /// <param name="_fDrawExtent"></param>
    private void DrawPolarGrid(float _fDrawExtent)
    {
        int iRingCount = Mathf.RoundToInt(_fDrawExtent / SnapUnit);
        for (int i = 1; i < iRingCount; i++) //跳過第一(0)個，因為第一個基本上只是最中心的一個點
        {
            Handles.DrawWireDisc(Vector3.zero, Vector3.up, i * SnapUnit);
        }

        for (int i = 0; i < AngularDivisions; i++)
        {
            //旋轉的程度 (百分比，0是剛開始，1是轉到底，360度)
            float fPercentage = i / (float)AngularDivisions;
            float fRadian = Mathf.PI * 2 * fPercentage;//取得當前弧度
            float fLineY = Mathf.Sin(fRadian);
            float fLineX = Mathf.Cos(fRadian);
            Vector3 targetPoint = new Vector3(fLineX, 0, fLineY);
            targetPoint = targetPoint.normalized * (iRingCount - 1) * SnapUnit;
            Handles.DrawPolyLine(Vector3.zero, targetPoint);
        }
    }

    void SnapSelections()
    {
        Debug.Log("Snap the things by window");
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            GameObject goTemp = Selection.gameObjects[i];
            Undo.RecordObject(goTemp.transform, "snap objects"); //請Undo開始監控你要的資料，若有變動的話它會將其記錄下來
            goTemp.transform.position
                = this.GetSnappedPos(goTemp.transform.position);
        }
    }
    Vector3 GetSnappedPos(Vector3 _originPos)
    {
        if (this.Type == EGridType.CARTESIAN) { return _originPos.RoundPos(this.SnapUnit); }
        else
        {
            Vector2 vecLookDown = new Vector2(_originPos.x, _originPos.z); //取得由上往下看的向量，因為我們不打算去動高度
            float distance = vecLookDown.magnitude; //取得與原點的距離
            float distSnapped = Snapper.RoundByUnit(distance, this.SnapUnit);

            float TAU = (Mathf.PI * 2);
            float fRadian = Mathf.Atan2(vecLookDown.y, vecLookDown.x); //取得當前旋轉弧度
            float fTurn = fRadian / TAU; //取得現在轉的比例(占整圈的幾%)
            float fTurnSnapped = Snapper.RoundByUnit(fTurn, 1f / AngularDivisions);
            float fRadianSnapped = fTurnSnapped * TAU;

            Vector2 dirSnapped = new Vector2(Mathf.Cos(fRadianSnapped), Mathf.Sin(fRadianSnapped));
            Vector2 snappedVec = dirSnapped * distSnapped;

            return new Vector3(snappedVec.x, _originPos.y, snappedVec.y);
        }
    }
}
