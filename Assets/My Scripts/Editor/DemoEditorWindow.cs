﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable]
public class DemoEditorWindow : EditorWindow //試製編輯器視窗
{
    [MenuItem("Tools / Demo")]
    public static void OpenTheThing() => GetWindow<DemoEditorWindow>("爹摸視窗");
    //上面這裡讓你在編輯器中新增Tools->Demo的選項路徑
    //按下去後會打開一個標題為"爹摸視窗"的視窗

    public enum EGridType
    {
        CARTESIAN, //笛卡爾座標 (方格狀的那種)
        POLAR //極座標 (圓圈狀)
    }

    [Range(0.5f, 5f)]
    public float SnapUnit = 0.5f;
    public EGridType Type;
    public int AngularDivisions = 24; //極座標當中，將圓圈切成幾等分

    private SerializedObject m_serializedObj;
    private SerializedProperty m_propSnapUnit;
    private SerializedProperty m_propGridType;
    private SerializedProperty m_propAngularDivision;

    const float rayMax = 300;

    private void OnEnable()
    {
        m_serializedObj = new SerializedObject(this); //將自己帶入序列化物件(記得要class上方標註[Serializable])
        //下面一一去將序列化後的變數欄位取出 (後面名字記得要打得跟你變數名稱一樣)
        m_propSnapUnit = m_serializedObj.FindProperty("SnapUnit");
        m_propGridType = m_serializedObj.FindProperty("Type");
        m_propAngularDivision = m_serializedObj.FindProperty("AngularDivisions");

        SceneView.onSceneGUIDelegate += this.DuringSceneGUI;//向SceneView註冊一個事件
        //SceneView那邊更新時會來呼叫你的DuringSceneGUI函式
    }
    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= this.DuringSceneGUI;//關閉時記得撤回先前註冊的事件
    }

    private void OnGUI()
    {
        m_serializedObj.Update(); //更新序列化物件看到的資料
        EditorGUILayout.PropertyField(m_propGridType);
        EditorGUILayout.PropertyField(m_propSnapUnit);
        if (this.Type == EGridType.POLAR) //只有在座標類型為極座標的時候才去繪製AngularDivision欄位
        {
            EditorGUILayout.PropertyField(m_propAngularDivision);
            if (m_propAngularDivision.intValue < 4) { m_propAngularDivision.intValue = 4; }
        }
        m_serializedObj.ApplyModifiedProperties(); //將視窗中的資料變化附加回原本的物件當中(也會順便請Undo系統紀錄)

        using (new EditorGUI.DisabledScope(Selection.gameObjects.Length <= 0))
        {//加入disabledScope，用以檢測說這個UI是否不能用
            if (GUILayout.Button("Snap Selection"))
            {
                this.SnapSelections();
            }
        }
    }

    void DuringSceneGUI(SceneView _sv)
    {
        if (Event.current.type == EventType.Repaint) //為避免繪製太多次，我們只在事件為Repaint時執行
        {//(Repaint基本上每一幀都會發生)
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual; //改為只有在線條沒有被擋住時才繪製
            //這樣寫代表只有你離Camera比較近時(<=)才會繪製

            switch (this.Type)
            {
                case EGridType.CARTESIAN:
                    Vector2 maxXY = Vector2.zero;
                    Vector2 minXY = Vector2.zero;
                    Plane yZeroPlane = new Plane(Vector3.up, Vector3.zero); //製造用來感應y=0位置的平面

                    Ray topLeft = HandleUtility.GUIPointToWorldRay(Vector2.zero); //取得左上角打向遊戲場景的射線 (注意這邊位置左上角為0,0，往下y增加)
                    if (yZeroPlane.Raycast(topLeft, out float fDis)) //檢測先前的平面會不會與此射線交集
                    {//有交集後會在out那邊回傳交及時的射線距離
                        maxXY = GetEdgeVector(topLeft.GetPoint(fDis), maxXY, true); //用GetPoint取得交集時的座標點，並帶入計算是否為最大或最小
                        minXY = GetEdgeVector(topLeft.GetPoint(fDis), minXY, false);
                    }
                    Ray topRight = HandleUtility.GUIPointToWorldRay(new Vector2(Screen.width, 0));
                    if (yZeroPlane.Raycast(topRight, out fDis))
                    {
                        maxXY = GetEdgeVector(topRight.GetPoint(fDis), maxXY, true);
                        minXY = GetEdgeVector(topRight.GetPoint(fDis), minXY, false);
                    }
                    Ray botRight = HandleUtility.GUIPointToWorldRay(new Vector2(Screen.width, Screen.height));
                    if (yZeroPlane.Raycast(botRight, out fDis))
                    {
                        maxXY = GetEdgeVector(botRight.GetPoint(fDis), maxXY, true);
                        minXY = GetEdgeVector(botRight.GetPoint(fDis), minXY, false);
                    }
                    Ray botLeft = HandleUtility.GUIPointToWorldRay(new Vector2(0, Screen.height));
                    if (yZeroPlane.Raycast(botLeft, out fDis))
                    {
                        maxXY = GetEdgeVector(botLeft.GetPoint(fDis), maxXY, true);
                        minXY = GetEdgeVector(botLeft.GetPoint(fDis), minXY, false);
                    }
                    this.DrawCartesianGrid(maxXY, minXY);
                    break;
                case EGridType.POLAR:
                    this.DrawPolarGrid(16);
                    break;
            }
        }
    }

    private void SnapSelections()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            GameObject goTemp = Selection.gameObjects[i];
            Undo.RecordObject(goTemp.transform, "snap objects");
            //請Undo開始監控你要的資料，若有變動的話它會將其記錄下來
            goTemp.transform.position = this.GetSnappedPos(goTemp.transform.position);
        }
    }

    /// <summary>
    /// 依照笛卡爾座標(方形)去繪製校正點
    /// </summary>
    /// <param name="_fDrawExtent"></param>
    private void DrawCartesianGrid(float _fDrawExtent)
    {
        int iLineCount = Mathf.RoundToInt((_fDrawExtent * 2) / SnapUnit);
        if (iLineCount % 2 == 0) { iLineCount++; }

        for (int i = 0; i < iLineCount; i++)
        {
            float xPoint = 0 - ((iLineCount / 2) * SnapUnit) + (SnapUnit * i);
            float zStart = 0 - _fDrawExtent;
            float zEnd = 0 + _fDrawExtent;
            Vector3 startPoint = new Vector3(xPoint, 0, zStart);
            Vector3 endPoint = new Vector3(xPoint, 0, zEnd);
            Handles.DrawPolyLine(startPoint, endPoint);
            startPoint = new Vector3(zStart, 0, xPoint);
            endPoint = new Vector3(zEnd, 0, xPoint);
            Handles.DrawPolyLine(startPoint, endPoint);
        }
    }
    /// <summary>
    /// 依照笛卡爾座標(方形)去繪製校正點 (帶入繪製範圍)
    /// </summary>
    /// <param name="_fDrawExtent"></param>
    private void DrawCartesianGrid(Vector2 _maxXY, Vector2 _minXY)
    {//帶入繪製範圍的最大&最小值，接著其實就跟先前寫的東西很像
        float fXRange = _maxXY.x - _minXY.x;
        float fZRange = _maxXY.y - _minXY.y;
        int iLineCountX = Mathf.RoundToInt(fXRange / this.SnapUnit);
        if (iLineCountX % 2 == 0) { iLineCountX++; }
        int iLineCountZ = Mathf.RoundToInt(fZRange / this.SnapUnit);
        if (iLineCountZ % 2 == 0) { iLineCountZ++; }
        float fStartPosX = Snapper.RoundByUnit(_minXY.x, this.SnapUnit);
        float fStartPosZ = Snapper.RoundByUnit(_minXY.y, this.SnapUnit);

        for (int i = 0; i < iLineCountX; i++) //繪製X軸上的線
        {
            Vector3 startPoint = new Vector3(fStartPosX + (this.SnapUnit * i), 0, _minXY.y);
            Vector3 endPoint = new Vector3(fStartPosX + (this.SnapUnit * i), 0, _maxXY.y);
            Handles.DrawPolyLine(startPoint, endPoint);
        }
        for (int i = 0; i < iLineCountZ; i++)//繪製Z軸上的線
        {
            Vector3 startPoint = new Vector3(_minXY.x, 0, fStartPosZ + (this.SnapUnit * i));
            Vector3 endPoint = new Vector3(_maxXY.x, 0, fStartPosZ + (this.SnapUnit * i));
            Handles.DrawPolyLine(startPoint, endPoint);
        }
    }

    private void DrawPolarGrid(float _fDrawExtent)
    {
        int iRingCount = Mathf.RoundToInt(_fDrawExtent / SnapUnit);
        for (int i = 1; i < iRingCount; i++) //繪製圓圈
        {//跳過第一(0)個，因為第一個基本上只是最中心的一個點，無需繪製
            Handles.DrawWireDisc(Vector3.zero, Vector3.up, i * SnapUnit);
        }
        for (int i = 0; i < this.AngularDivisions; i++) //繪製分割線
        {
            //旋轉的程度 (百分比，0是剛開始，1是轉到底——360度)
            float fPercentage = i / (float)AngularDivisions;
            float fRadian = Mathf.PI * 2 * fPercentage;//取得當前弧度
            float fLineY = Mathf.Sin(fRadian);
            float fLineX = Mathf.Cos(fRadian);
            Vector3 targetPoint = new Vector3(fLineX, 0, fLineY);
            targetPoint = targetPoint.normalized * (iRingCount - 1) * SnapUnit;
            Handles.DrawPolyLine(Vector3.zero, targetPoint);
        }
    }

    Vector3 GetSnappedPos(Vector3 _originPos)
    {
        Vector3 outPutPos = Vector3.zero;
        switch (this.Type)
        {
            case EGridType.CARTESIAN:
                outPutPos = _originPos.RoundPos(this.SnapUnit);
                break;
            case EGridType.POLAR:
                Vector2 vecLookDown = new Vector2(_originPos.x, _originPos.z); //取得由上往下看的向量，高度的部份我們要單獨處理
                float distance = vecLookDown.magnitude; //取得與原點的距離
                float distSnapped = Snapper.RoundByUnit(distance, this.SnapUnit); //轉換成校正後的距離

                float TAU = (Mathf.PI * 2); //一整圈 = 360角度 = (2*PI)弧度
                float fRadian = Mathf.Atan2(vecLookDown.y, vecLookDown.x); //取得當前旋轉弧度
                float fTurn = fRadian / TAU; //取得現在轉的比例(占整圈的幾%)
                float fTurnSnapped = Snapper.RoundByUnit(fTurn, 1f / AngularDivisions); //對旋轉比例進行校正
                float fRadianSnapped = fTurnSnapped * TAU; //將旋轉比例轉換成確切的弧度

                Vector2 dirSnapped = new Vector2(Mathf.Cos(fRadianSnapped), Mathf.Sin(fRadianSnapped));
                Vector2 snappedVec = dirSnapped * distSnapped;
                float fSnappedPosY = Snapper.RoundByUnit(_originPos.y, this.SnapUnit);

                outPutPos = new Vector3(snappedVec.x, fSnappedPosY, snappedVec.y);
                break;
        }
        return outPutPos;
    }

    private Vector2 GetEdgeVector(Vector3 _pos, Vector2 _originVector, bool _bGetMax = true)
    { //取得最大或最小值，後面的參數預設為true(取最大)
        Vector2 output = _originVector;
        if (_bGetMax)
        {
            if (_pos.x > _originVector.x) { output.x = _pos.x; }
            if (_pos.z > _originVector.y) { output.y = _pos.z; }
        }
        else
        {
            if (_pos.x < _originVector.x) { output.x = _pos.x; }
            if (_pos.z < _originVector.y) { output.y = _pos.z; }
        }
        return output;
    }

    private void DrawPolarGrid(Vector2 _maxXY, Vector2 _minXY)
    {
        /*需要
         * 範圍 (依照你的SnapUnit去確認你需要在這個區間內生成幾個圓圈)
         * 線條起點/終點 (線的繪製範圍，需要用ray來判斷)
         * 在你畫面的周遭宣告邏輯上的Plane
         * 然後從圓心點打出數道射線，檢查有沒有射到你的邊框處
         */
        Vector3 planeVector = new Vector3(_maxXY.x - _minXY.x, 0, 0);
        //Plane planeRight = new Plane(Vector3 , )

    }
}