﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects] //宣告說此自製Editor可一次選取多個並編輯
[CustomEditor(typeof(SupplyCube))] //標記該自訂編輯器是針對SupplyCube類別設計的
public class SupplyCubeEditor : Editor //自訂編輯器需繼承自Editor
{
    SerializedObject m_serializedObj; //宣告一個主要觀測的序列化物件
    //下面再去宣告多個要觀測的變數
    SerializedProperty m_propHealValue;
    SerializedProperty m_propRadius;
    SerializedProperty m_propUselessValue;
    SerializedProperty m_propType;

    private void OnEnable()
    {
        m_serializedObj = serializedObject; //serializedObject可以讓你直接取得當前CustomEditor對應的類別
        m_propHealValue = m_serializedObj.FindProperty("m_fHealValue"); //記得參數要寫的跟你的變數名稱一樣
        m_propRadius = m_serializedObj.FindProperty("m_fRadius");
        m_propUselessValue = m_serializedObj.FindProperty("m_fUselessValue");
        m_propType = m_serializedObj.FindProperty("m_type");
    }

    public override void OnInspectorGUI()
    {//OnInspectorGUI跟先前的OnDrawGizmos有點像，不過這裡指的是視窗中的每幀更新，而非遊戲場景

        m_serializedObj.Update(); //更新序列化物件這邊看到的資料
        //我自己實驗的話一般狀況下好像有沒有Update都沒差
        //但查提問有人說可能會發生兩個序列化物件去存取同一個物件資料的狀況，因此需要確保他們看到的是最新狀態
        //提問:https://answers.unity.com/questions/1455633/why-serializedpropertyupdate-is-called-in-the-begi.html

        EditorGUILayout.PropertyField(m_propType);
        EditorGUILayout.PropertyField(m_propHealValue);
        if (m_propType.enumValueIndex == (int)ESupplyType.AREA) { EditorGUILayout.PropertyField(m_propRadius); }
        //判斷說只有當你是範圍型補包的時候才顯示半徑的欄位

        EditorGUILayout.PropertyField(m_propUselessValue);
        m_serializedObj.ApplyModifiedProperties(); //將我們在Inspector裡做的數值調動附加到原物件

        GUILayout.Label("這是一個自訂編輯器");
        //為了確認已經將SupplyCube的編輯器視窗改為我們自訂的版本，我們在下面先加一行文字
    }
}