﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class Ripple : MonoBehaviour
{

    SpriteRenderer mSpriteRenderer;
    Collider2D mCircleCollider;

    //private Color[] m_arrClr = new Color[] { Color.blue , Color.red , Color.gray , Color.green };

    void Awake()
    {
        mSpriteRenderer = transform.GetComponent<SpriteRenderer>();
        mCircleCollider = transform.GetComponent<Collider2D>();
    }
    // Use this for initialization
    void Start()
    {
        //Invoke("unenbaleTrigger", 0.05f);//片刻後關閉碰撞判定，並不是很必要的感覺

        mSpriteRenderer.material.SetFloat("_StartTime", Time.time); //設定漣漪Shader的起始時間

        #region 實驗Shader變數是否可個別變動 (實驗結果為可以)
        //int iRandNum = Random.Range(0, m_arrClr.Length);
        //mSpriteRenderer.material.SetColor("_OutputClr", m_arrClr[iRandNum] );
        #endregion 實驗Shader變數是否可個別變動 (實驗結果為可以)

        float animationTime = mSpriteRenderer.material.GetFloat("_AnimationTime");
        float destroyTime = animationTime;
        //需要減去起始位置所需要消耗的時間 (如此一來在水波紋跑完的時候就會直接消去物件)
        destroyTime -= mSpriteRenderer.material.GetFloat("_StartWidth") * animationTime;
        Destroy(transform.gameObject, destroyTime);

        //float fLifeTIme = mSpriteRenderer.material.GetFloat("_AnimationTime");
        //Destroy(this.gameObject, fLifeTIme); //我自己是想說用的簡單些
    }

    public void unenbaleTrigger()
    {
        mCircleCollider.enabled = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

    }

}