﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteAlways] //特別注意，有些東西如果連編輯模式都一直跑可能會出事(例如生成物件)
public class ExplosiveBarrel : MonoBehaviour
{
    /// <summary>
    /// 把Shader的顏色屬性ID存起來
    /// </summary>
    static readonly int shPropClrID = Shader.PropertyToID("_Color");

    public BarrelData data;

    MaterialPropertyBlock m_mpb;
    MaterialPropertyBlock Mpb
    {
        get
        {
            if (m_mpb == null) { m_mpb = new MaterialPropertyBlock(); }
            return m_mpb;
        }
    }

    private void Awake()
    {
        m_mpb = new MaterialPropertyBlock();

        //    Shader shader = Shader.Find("Default/Diffuse");
        //    Material mat = new Material(shader) { hideFlags = HideFlags.HideAndDontSave };
        //生成一個材質並將其設為不顯示在Hierarchy，並且也不要存起來
        //參考:https://docs.unity3d.com/ScriptReference/HideFlags.html

        //呼叫時會額外生成一個材質
        //，導致物件無法整理到同一個Draw call完成，也同時浪費記憶體
        //this.GetComponent<MeshRenderer>().material.color = m_clr;

        //這麼做會直接去改動到實際資料夾中的材質
        //this.GetComponent<MeshRenderer>().sharedMaterial.color = m_clr;
    }
    //實驗證實[ExcuteAlways]可以讓Update在編輯模式下一直被呼叫
    //private void Update()
    //{
    //    if (Application.isEditor)
    //    {
    //        Debug.Log("在編輯模式");
    //    }
    //    else if (EditorApplication.isPlaying)
    //    {
    //        Debug.Log("在Play模式");
    //    }
    //    //this.transform.position += (Vector3.right * Time.deltaTime * 0.5f);
    //}

    [ContextMenu("做點什麼")]
    public void DoSomething() //測試，這樣子可以讓你直接在Inspector那邊對著Class右鍵執行function
    {
        this.transform.position += Vector3.up;
        Debug.Log("做點什麼");
    }

    public void ApplyColor(Color _clr)
    {
        MeshRenderer rnd = this.GetComponent<MeshRenderer>();
        Mpb.SetColor(shPropClrID, _clr);
        rnd.SetPropertyBlock(m_mpb);
    }
    public void ApplyColor() { this.ApplyColor(data.m_clr); }

    private void OnValidate() //每當Inspector那邊屬性變更時呼叫 (只在編輯器當中有效)
    {
        if (this.data != null)
        {
            this.ApplyColor(data.m_clr);
        }
    }

    //講者常用的一個技巧，不過這些function只有在執行時才會被呼叫，因此你可以加入[ExecuteAlways]
    private void OnEnable() { ExplosiveBarrelMgr.AllBarrels.Add(this);}
    private void OnDisable() { ExplosiveBarrelMgr.AllBarrels.Remove(this);}

    private void OnDrawGizmos()//類似在編輯器當中顯示的Update
    {
        this.GizmosDraw();
    }
    //private void OnDrawGizmosSelected()//改為只有在被挑選的時候才會去繪製
    //{
    //    this.GizmosDraw();
    //}

    private void GizmosDraw()
    {
        if (this.data == null) { return; }

#if UNITY_EDITOR
        Handles.color = data.m_clr; //繪製時特別依照自己的顏色去繪製
        Handles.DrawWireDisc(this.transform.position, this.transform.up, data.m_fRadius);
        Handles.color = Color.white;
#endif

        //Gizmos.DrawWireSphere(this.transform.position, m_fRadius);
        //以自己的位置為圓心，繪製一個圓圈
    }
}
