﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// 補給品的種類，可分為範圍型和單體
/// </summary>
public enum ESupplyType
{
    AREA,
    SINGLE
}

public class SupplyCube : MonoBehaviour
{
    [SerializeField]
    private ESupplyType m_type;
    [SerializeField] //標記說該變數一定要序列化(顯示在Inspector當中)
    private float m_fHealValue = 5;

    [Range(2.5f , 10)] //限制你在Inspector當中可調整的範圍
    public float m_fRadius = 2.5f;
    [Header("這個變數沒啥幹用")] //在Inspector當中加入說明文字
    [Space(10)]//並且使其與前一個變數欄位相隔10單位
    public float m_fUselessValue = 69;

    private void OnDrawGizmos()//類似在編輯器當中顯示的Update
    {
        if (this.m_type == ESupplyType.AREA) //只有當補包類型為範圍型的時候才繪製半徑
        {
            Color clrTemp = Gizmos.color; //先把Gizmos原先的顏色存起來
            Gizmos.color = Color.blue;//變換Gizmos現在的繪製顏色
            Gizmos.DrawWireSphere(this.transform.position, this.m_fRadius);
            Gizmos.color = clrTemp; //繪製完成後記得調回來

            clrTemp = Handles.color;
            Handles.color = Color.yellow;
            Handles.DrawWireDisc(this.transform.position, this.transform.up, this.m_fRadius * 1.2f);
            //以自己的位置為圓心，繪製一個面朝上的圓圈
            Handles.color = clrTemp;
        }
    }
}