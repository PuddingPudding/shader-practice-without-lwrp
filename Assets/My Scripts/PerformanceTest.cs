﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Profiling;

public class PerformanceTest : MonoBehaviour
{
    public Camera m_mainCam;
    public GameObject m_testPrefab;
    public int m_iTestingTIme = 1000;
    string m_str = "";
    CustomSampler m_sampler;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(this.TestFunction(m_iTestingTIme , 0.5f));
        m_sampler = CustomSampler.Create("Pudding Testing");
    }
    
    IEnumerator TestFunction(int _iTestTime = 100, float _fWaitTime = 1)
    {
        yield return new WaitForSeconds(_fWaitTime);
        m_sampler.Begin();

        //Camera.main.orthographicSize = 7; //測試Camera.main和原本就有參照時的效能和GC差異
        //this.m_mainCam.orthographicSize = 7; //測試起來差不多

        List<GameObject> listTesting = new List<GameObject>();
        GameObject objTemp;
        //Transform transformTemp;
        for (int i = 0; i < _iTestTime; i++)
        {
            /*GameObject*/ objTemp = GameObject.Instantiate(m_testPrefab);
            //試著不要每次迴圈都宣告一個GameObject變數，測試後發現對GC沒有幫助的樣子
            objTemp.SetActive(true);
            //transformTemp = objTemp.transform;
            objTemp.transform.localPosition += new Vector3(Random.Range(-2.3f, 2.3f)
                , Random.Range(-2.3f, 2.3f), Random.Range(-2.3f, 2.3f));
            //transformTemp.localPosition += new Vector3(Random.Range(-2.3f, 2.3f)
                //, Random.Range(-2.3f, 2.3f), Random.Range(-2.3f, 2.3f));
            listTesting.Add(objTemp);
        }
        m_sampler.End();
        yield return new WaitForSeconds(_fWaitTime);
        while (listTesting.Count > 0)
        {
            /*GameObject*/ objTemp = listTesting[0];
            listTesting.RemoveAt(0);
            //objTemp.SetActive(true);
            //objTemp.SetActive(false);
            //GameObject.Destroy(objTemp);
        }

        string strTemp = "";
        StringBuilder sb = new StringBuilder("", _iTestTime);
        //string存在heap，StringBuilder存在stack，後者用完就直接歸還，前者則會先標記為垃圾，使的前者GC消耗較大
        for (int i = 0; i < _iTestTime; i++)
        {
            //m_str += "a";
            strTemp += "b";
            sb.Append('c');
        }
    }
}
