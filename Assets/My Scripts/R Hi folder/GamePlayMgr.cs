﻿using UnityEngine;

namespace Rayark.Hi
{
    using Engine;

    public class GamePlayMgr : MonoBehaviour
    {
        [SerializeField]
        private CharacterData m_characterData;
        [SerializeField]
        private Transform m_characterTransform;

        private HiEngine m_hiEngine;

        // Start is called before the first frame update
        void Start()
        {
            m_hiEngine = new HiEngine(m_characterData);
        }

        // Update is called once per frame
        void Update()
        {
            m_hiEngine.Update(Time.deltaTime);
            m_characterTransform.localPosition = new Vector3(
                m_characterTransform.localPosition.x,
                m_characterTransform.localPosition.y,
                m_hiEngine.CurrentCharacterPosition.y
                );
        }
    }
}