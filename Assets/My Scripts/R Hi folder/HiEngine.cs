﻿using UnityEngine;

namespace Rayark.Hi.Engine
{
    public class HiEngine
    {
        private CharacterData m_currentCharacter;

        public Vector2 CurrentCharacterPosition
        {
            get { return m_currentCharacter.Position; }
        }

        public HiEngine(CharacterData _characterData)
        {
            m_currentCharacter = _characterData;
        }
        public void Update(float _fDeltaTime)
        {
            m_currentCharacter.Position.y += _fDeltaTime * m_currentCharacter.Speed;
        }
    }
}