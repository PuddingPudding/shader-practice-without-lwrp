﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pudding70119.ForResume
{
    public class ChangeAllShader : MonoBehaviour
    {
        public enum EShaderType
        {
            Fresnel,
            Dissolve
        }

        [SerializeField]
        private Shader m_shaderForReplace;

        [SerializeField]
        private Transform m_meshRoot;

        [SerializeField]
        private EShaderType m_shaderType;

        [SerializeField]
        private Texture2D m_noiseTex;

        // Start is called before the first frame update
        void Start()
        {
            if(m_meshRoot == null) { m_meshRoot = this.transform; }
            SkinnedMeshRenderer[] allMeshRenderer = m_meshRoot.GetComponentsInChildren<SkinnedMeshRenderer>();
            Debug.Log("meshRenderer count: " + allMeshRenderer.Length);
            for (int i = 0; i < allMeshRenderer.Length; i++)
            {
                for(int j = 0; j < allMeshRenderer[i].materials.Length; j++)
                {
                    allMeshRenderer[i].materials[j].shader = this.m_shaderForReplace;
                    switch(this.m_shaderType)
                    {
                        case EShaderType.Fresnel:
                            allMeshRenderer[i].materials[j].SetColor("_HighlightClr", new Color(10/255f, 230/255f, 150/255f));
                            allMeshRenderer[i].materials[j].SetFloat("_FresnelPwr", 1.5f);
                            break;
                        case EShaderType.Dissolve:
                            allMeshRenderer[i].materials[j].SetTexture("_NoiseTex", this.m_noiseTex);
                            allMeshRenderer[i].materials[j].SetFloat("_CustomStyle", 1);
                            allMeshRenderer[i].materials[j].SetColor("_EdgeColour1", new Color(0.85f , 0.6f , 0.1f));
                            allMeshRenderer[i].materials[j].SetColor("_EdgeColour2", new Color(0.85f, 0.2f,0));
                            allMeshRenderer[i].materials[j].SetFloat("_Level", 0.45f);
                            allMeshRenderer[i].materials[j].SetFloat("_Edges", 0.06f);
                            break;
                    }
                }
            }
        }

        //// Update is called once per frame
        //void Update()
        //{

        //}
    }
}