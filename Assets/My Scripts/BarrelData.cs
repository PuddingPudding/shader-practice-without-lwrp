﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BarrelData : ScriptableObject
{
    [Range(1f, 8f)]
    public float m_fRadius = 1;
    public float m_fDmg = 10;
    public Color m_clr = Color.red;
}
