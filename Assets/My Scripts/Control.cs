﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Control : MonoBehaviour
{

    public GameObject prefabRippleEffect;
    public float distance;
    // Use this for initialization
    void Start()
    {
        distance = transform.localPosition.z - Camera.main.transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        CheckTap();
    }
    void CheckTap()
    {

        if (Input.GetMouseButtonDown(0))
        {

            var dian = Input.mousePosition;
            Debug.Log("按到的點位" + dian);
            CreateNewRipple(dian);
        }

    }
    void CreateNewRipple(Vector2 pos)
    {
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, distance)); //根據Camera位置生成物件
        Debug.Log("物體位置" + worldPos);
        GameObject tem = Instantiate(prefabRippleEffect);
        tem.transform.localPosition = new Vector3(worldPos.x, worldPos.y, 0);
        tem.transform.SetParent(transform, true);
    }
}