﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SimpleBlit : MonoBehaviour
{
    public Material TransitionMaterial;

    void OnRenderImage(RenderTexture src, RenderTexture dst) //每次渲染完成後呼叫
    {
        if (TransitionMaterial != null)
            Graphics.Blit(src, dst, TransitionMaterial); //用這個方法可以把Camera看到的東西套到Shader上
    }
}
